package com.gdn.qa.future;

import net.serenitybdd.junit.spring.integration.SpringIntegrationMethodRule;
import net.thucydides.core.steps.ScenarioSteps;
import org.junit.Rule;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = Config.class)
@SpringBootTest
public abstract class BaseStepDefinition extends ScenarioSteps {
    @Rule
    public SpringIntegrationMethodRule springIntegrationMethodRule = new SpringIntegrationMethodRule();
}
