package com.gdn.qa.future.steps.mobile;

import com.gdn.qa.future.BaseStepDefinition;
import com.gdn.qa.future.data.ui.ProductData;
import com.gdn.qa.future.pages.mobile.LamaBerlanggananMobilePage;
import com.gdn.qa.future.pages.mobile.ProductDetailPageMobile;
import com.gdn.qa.future.pages.mobile.SubscriptionCartMobilePage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.hamcrest.Matchers;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;

public class SubscribeMobileSteps extends BaseStepDefinition {
    @Autowired
    private LamaBerlanggananMobilePage lamaBerlanggananMobilePage;

    @Autowired
    private ProductDetailPageMobile productDetailPageMobile;

    @Autowired
    private SubscriptionCartMobilePage subscriptionCartMobilePage;

    @Autowired
    private ProductData productData;

    @When("[Subscription-UI-Mobile] user click subscription button")
    public void subscriptionUIMobileUserClickSubscriptionButton() {
        productDetailPageMobile.clickBerlanggananButton();
    }

    @And("[Subscription-UI-Mobile] user click siap on subscription onboarding if exist")
    public void subscriptionUIMobileUserClickSiapOnSubscriptionOnboardingIfExist() {
        productDetailPageMobile.clickSiapOnOnboardingSubs();
    }

    @And("[Subscription-UI-Mobile] user choose paket langganan with {string}")
    public void subscriptionUIMobileUserChoosePaketLanggananWithValues(String value) {
        lamaBerlanggananMobilePage.fillLamaBerlangganan(value);
    }

    @And("[Subscription-UI-Mobile] user fill quantity with {string}")
    public void subscriptionUIMobileUserFillQuantityWithQuantity(String qty) {
        lamaBerlanggananMobilePage.fillQuantity(qty);
    }

    @And("[Subscription-UI-Mobile] user click tambah ke langganan button")
    public void subscriptionUIMobileUserClickTambahKeLanggananButton() {
        lamaBerlanggananMobilePage.clickTambahKeLanggananButton();
    }

    @Then("[Subscription-UI-Mobile] user should see the subscription success's popup")
    public void subscriptionUIMobileUserShouldSeeTheSubscriptionSuccessSPopup() {
        assertThat("subscription success popup doesnt show", lamaBerlanggananMobilePage.getSuccessSubsribePopup(),
                Matchers.equalTo(true));
    }

    @And("[Subscription-UI-Mobile] user should see lihat langganan button is clickable")
    public void subscriptionUIMobileUserShouldSeeLihatLanggananButtonIsClickable() {
        assertThat("lihat langganan button not clickable", lamaBerlanggananMobilePage.getLihatLanggananButton(),
                Matchers.equalTo(true));
    }

    @When("[Subscription-UI-Mobile] user click lihat langganan button")
    public void subscriptionUIMobileUserClickLihatLanggananButton() {
        lamaBerlanggananMobilePage.clickLihatLanggananButton();
    }
}
