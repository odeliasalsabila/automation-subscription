package com.gdn.qa.future.steps.mobile;

import com.gdn.qa.future.BaseStepDefinition;
import com.gdn.qa.future.data.ui.UserData;
import com.gdn.qa.future.pages.mobile.AccountMobilePage;
import com.gdn.qa.future.pages.mobile.BlibliHomePageMobile;
import com.gdn.qa.future.pages.mobile.LoginMobilePage;
import com.gdn.qa.future.properties.UserProperties;
import com.gdn.qa.future.utils.csvUtil;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.hamcrest.Matchers;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class LoginMobile extends BaseStepDefinition {
    @Autowired
    private BlibliHomePageMobile blibliHomePageMobile;

    @Autowired
    private LoginMobilePage loginMobilePage;

    @Autowired
    private UserData userData;

    @Autowired
    private UserProperties userProperties;

    @Autowired
    private AccountMobilePage accountMobilePage;

    @Autowired
    private csvUtil openCsvUtil;

    @Given("[Subscription-UI-Mobile] user open blibli app")
    public void subscriptionUIMobileuserOpenBlibliApp() {
        blibliHomePageMobile.openBlibliApp();
    }

    @When("[Subscription-UI-Mobile] user click masuk")
    public void subscriptionUIMobileuserClickMasuk() {
        blibliHomePageMobile.clickLoginMenu();
    }

    @And("[Subscription-UI-Mobile] user type {string} and {string}")
    public void subscriptionUIMobileUserTypeUsernameAndPassword(String emailTxt, String passwordTxt) {
        if (emailTxt.equalsIgnoreCase("userEmail")){
            userData.setEmailUser(userProperties.getEmail());
        }else{
            userData.setEmailUser(emailTxt);
        }

        if (passwordTxt.equalsIgnoreCase("userPassword")){
            userData.setPasswordUser(userProperties.getPassword());
        }else{
            userData.setPasswordUser(passwordTxt);
        }

        loginMobilePage.typeUsername(userData.getEmailUser());
        loginMobilePage.typePassword(userData.getPasswordUser());
    }

    @And("[Subscription-UI-Mobile] user click akun")
    public void subscriptionUIMobileUserClickAkun() {
        accountMobilePage.clickAkun();
    }

    @And("[Subscription-UI-Mobile] user click user info")
    public void subscriptionUIMobileUserClickUserInfo() {
        accountMobilePage.clickUserInfo();
    }

    @And("[Subscription-UI-Mobile] user click login button form")
    public void subscriptionUIMobileUserClickLoginButtonForm() {
        loginMobilePage.clickLoginButtonForm();
    }

    @And("[Subscription-UI-Mobile] user click email validation button")
    public void subscriptionUIMobileUserClickEmailValidationButton() {
        loginMobilePage.clickEmailValidation();
    }

    @And("[Subscription-UI-Mobile] user fill the code of email validation")
    public void subscriptionUIMobileUserFillTheCodeOfEmailValidation() {
        loginMobilePage.fillCodeValidation(userData.getVerificationCode());
    }

    @And("[Subscription-UI-Mobile] user click verifikasi button of email verification form")
    public void subscriptionUIMobileUserClickVerifikasiButtonOfEmailVerificationForm() {
        loginMobilePage.clickVerificationBtn();
    }

    @And("[Subscription-UI-Mobile] user click nanti saja on verification page")
    public void subscriptionUIMobileUserClickNantiSajaOnVerificationPage() {
        loginMobilePage.clickNantiSaja();
    }

    @And("[Subscription-UI-Mobile] user click sure on verification page")
    public void subscriptionUIMobileUserClickSureOnVerificationPage() {
        loginMobilePage.clickSureVerification();
    }

    @Then("[Subscription-UI-Mobile] validate user is already login as previous email filled in")
    public void subscriptionUIMobileValidateUserIsAlreadyLoginAsPreviousEmailFilledIn() {
        assertThat("email doesnt match", accountMobilePage.getUserName(),
                equalTo(userData.getEmailUser()));
    }

    @Then("[Subscription-UI-Mobile] user should see {string} for {string}")
    public void subscriptionUIMobileUserShouldSeeErrorMessageForNegativeScenario(String errorMessage, String scenario) {
        assertThat("error message doesnt match for"+scenario, loginMobilePage.getErrorMessage(),
                Matchers.equalToIgnoringCase(errorMessage));
    }

    @Then("[Subscription-UI-Mobile] user should see the login button is disabled")
    public void subscriptionUIMobileUserShouldSeeTheLoginButtonIsDisabled() {
        assertThat("login button is enabled", loginMobilePage.loginButtonIsEnable(), equalTo(false));
    }
}
