package com.gdn.qa.future.steps.desktop;

import com.gdn.qa.future.BaseStepDefinition;
import com.gdn.qa.future.pages.desktop.LamaBerlanggananPage;
import io.cucumber.java.en.And;
import org.springframework.beans.factory.annotation.Autowired;

public class LamaBerlanggananSteps extends BaseStepDefinition {
    @Autowired
    private LamaBerlanggananPage lamaBerlanggananPage;

    @And("[Subscription-UI-Desktop] user choose paket langganan with {string}")
    public void subscriptionUIDesktopUserChoosePaketLanggananWithValues(String values) {
        lamaBerlanggananPage.fillLamaBerlangganan(values);
    }

    @And("[Subscription-UI-Desktop] user fill quantity with {string}")
    public void subscriptionUIDesktopUserFillQuantityWithQuantity(String quantity) {
        lamaBerlanggananPage.fillSubscriptionQUantity(Integer.parseInt(quantity));
    }

    @And("[Subscription-UI-Desktop] user click tambah ke langganan button")
    public void subscriptionUIDesktopUserClickTambahKeLanggananButton() {
        lamaBerlanggananPage.clickTambahKeLangganan();
    }

    @And("[Subscription-UI-Desktop] user click lihat langganan button")
    public void subscriptionUIDesktopUserClickLihatLanggananButton() {
        lamaBerlanggananPage.clickLihatLangganan();
    }
}
