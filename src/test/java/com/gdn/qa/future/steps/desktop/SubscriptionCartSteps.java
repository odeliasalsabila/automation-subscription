package com.gdn.qa.future.steps.desktop;

import com.gdn.qa.future.BaseStepDefinition;
import com.gdn.qa.future.api.request.SubscriptionCartRequest;
import com.gdn.qa.future.data.api.SubscriptionCartApiData;
import com.gdn.qa.future.pages.desktop.LamaBerlanggananPage;
import com.gdn.qa.future.pages.desktop.SubscriptionCartPage;
import com.gdn.qa.future.properties.ProductProperties;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

public class SubscriptionCartSteps extends BaseStepDefinition {
    @Autowired
    private SubscriptionCartApiData subscriptionCartApiData;

    @Autowired
    private ProductProperties productProperties;

    @Autowired
    private LamaBerlanggananPage lamaBerlanggananPage;

    @Autowired
    private SubscriptionCartPage subscriptionCartPage;

    @And("[Subscription-API-Desktop] user prepare data subscription cart with price from ui")
    public void subscriptionAPIDesktopUserPrepareDataSubscriptionCartWithPrice() {
        subscriptionCartApiData.setPrice(lamaBerlanggananPage.getPrice());
    }

    @And("[Subscription-API-Desktop] user prepare data subscription cart with quantity from ui")
    public void subscriptionAPIDesktopUserPrepareDataSubscriptionCartWithQuantityByQuantity() {
        subscriptionCartApiData.setQuantity(lamaBerlanggananPage.getQuantity());
    }

    @And("[Subscription-API-Desktop] user prepare data subscription cart with frequency from ui")
    public void subscriptionAPIDesktopUserPrepareDataSubscriptionCartWithFrequencyByFrequency() {
        subscriptionCartApiData.setFrequency(lamaBerlanggananPage.getFrequency());
    }

    @And("[Subscription-API-Desktop] user prepare data subscription cart with period from ui")
    public void subscriptionAPIDesktopUserPrepareDataSubscriptionCartWithPeriodByPeriod() {
        subscriptionCartApiData.setPeriod(lamaBerlanggananPage.getPeriod());
    }

    @Given("[Subscription-API-Desktop] user prepare data subscription cart with item sku by {string}")
    public void subscriptionAPIDesktopUserPrepareDataSubscriptionWithItemSkuByProductSku(String itemSku) {
        subscriptionCartApiData.setItemSku(productProperties.getItemSku().get(itemSku));
    }

    @When("[Subscription-UI-Desktop] user go to subscription cart page")
    public void subscriptionUIDesktopUserGoToSubscriptionCartPage() {
        subscriptionCartPage.open();
    }
}
