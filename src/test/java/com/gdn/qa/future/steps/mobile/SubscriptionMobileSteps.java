package com.gdn.qa.future.steps.mobile;

import com.gdn.qa.future.BaseStepDefinition;
import com.gdn.qa.future.pages.mobile.ProductDetailPageMobile;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.hamcrest.Matchers;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;

public class SubscriptionMobileSteps extends BaseStepDefinition {
    @Autowired
    private ProductDetailPageMobile productDetailPageMobile;

    @When("[Subscription-UI-Mobile] user click oke on onboarding subscription if exist")
    public void subscriptionUIMobileUserClickOkeOnOnboardingSubscriptionIfExist() {
        productDetailPageMobile.clickOkeOnOnboardingSubs();
    }

    @Then("[Subscription-UI-Mobile] user should see berlangganan button is clickable")
    public void subscriptionUIMobileUserShouldSeeBerlanggananButtonIsClickable() {
        assertThat("Berlangganan button is not clickable", productDetailPageMobile.getBerlanggananButton(),
                Matchers.equalTo(true));
    }

    @Then("[Subscription-UI-Mobile] user should not see those product subscription again")
    public void subscriptionUIMobileUserShouldNotSeeThoseProductSubscriptionAgain() {
    }
}
