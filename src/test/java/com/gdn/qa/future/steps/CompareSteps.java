package com.gdn.qa.future.steps;

import com.gdn.qa.future.BaseStepDefinition;
import com.gdn.qa.future.data.comparisonData.APIData;
import com.gdn.qa.future.data.comparisonData.MobileData;
import com.gdn.qa.future.data.comparisonData.UiData;
import com.gdn.qa.future.utils.csvUtil;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.SneakyThrows;
import org.hamcrest.Matchers;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;

public class CompareSteps extends BaseStepDefinition {
    @Autowired
    private csvUtil csvUtil;

    @Autowired
    private MobileData mobileData;

    @Autowired
    private UiData uiData;

    @Autowired
    private APIData apiData;

    @Then("value should each other")
    public void valueShouldEachOther() {
        assertThat("email value from api doesnt match with ui", apiData.getTitles(),
                Matchers.containsInAnyOrder(uiData.getTitles().toArray(new String[0])));
        assertThat("email value from api doesnt match with mobile", apiData.getTitles(),
                Matchers.containsInAnyOrder(mobileData.getTitles().toArray(new String[0])));
    }

    @When("[Subscription-API-Desktop] user get value from file {string}")
    public void subscriptionAPIDesktopUserGetValueFromFileResultAPI(String filename) {
        List<APIData> apiDataList = (List<APIData>) (Object) csvUtil.readFile(APIData.class, filename);
        apiData = apiDataList.get(0);
    }

    @And("[Subscription-UI-Desktop] user get value from file {string}")
    public void subscriptionUIDesktopUserGetValueFromFileResultUI(String filename) {
        List<UiData> uiDataList = (List<UiData>) (Object) csvUtil.readFile(UiData.class, filename);
        uiData = uiDataList.get(0);
    }

    @And("[Subscription-UI-Mobile] user get value from file {string}")
    public void subscriptionUIMobileUserGetValueFromFileResultMobile(String filename) {
        List<MobileData> mobileDataList = (List<MobileData>) (Object) csvUtil.readFile(MobileData.class, filename);
        mobileData = mobileDataList.get(0);
    }
}
