package com.gdn.qa.future.steps.desktop;

import com.gdn.qa.future.BaseStepDefinition;
import com.gdn.qa.future.api.MailReading;
import com.gdn.qa.future.data.ui.UserData;
import com.gdn.qa.future.properties.UserProperties;
import io.cucumber.java.en.And;
import org.springframework.beans.factory.annotation.Autowired;

public class MailSteps extends BaseStepDefinition {

    @Autowired
    MailReading mailReading;

    @Autowired
    UserData userData;

    @Autowired
    UserProperties userProperties;

    @And("[Google-Mail-API] user read the new inbox in mail using {string} and {string}")
    public void googleMailAPIUserReadTheNewInboxInMailUsingUserEmailAndUserPassword(String emailTxt, String passTxt) {
        if (emailTxt.equalsIgnoreCase("userEmail") && passTxt.equalsIgnoreCase("userPassword")){
            userData.setVerificationCode(mailReading.readVerificationCode(userProperties.getEmail(), userProperties.getPasswordGmail()));
        }
        else {
            userData.setVerificationCode(mailReading.readVerificationCode(emailTxt, passTxt));
        }
        System.out.println("OTP = " +userData.getVerificationCode());
    }
}
