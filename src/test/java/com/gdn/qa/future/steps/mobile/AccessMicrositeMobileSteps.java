package com.gdn.qa.future.steps.mobile;

import com.gdn.qa.future.BaseStepDefinition;
import com.gdn.qa.future.data.comparisonData.MobileData;
import com.gdn.qa.future.pages.mobile.FavoritPageMobile;
import com.gdn.qa.future.pages.mobile.MicrositePageMobile;
import com.gdn.qa.future.pages.mobile.TnCSubsPageMobile;
import com.gdn.qa.future.utils.csvUtil;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.hamcrest.Matchers;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;

public class AccessMicrositeMobileSteps extends BaseStepDefinition {
    @Autowired
    private FavoritPageMobile favoritPageMobile;

    @Autowired
    private MicrositePageMobile micrositePageMobile;

    @Autowired
    private TnCSubsPageMobile tnCSubsPageMobile;

    @Autowired
    private csvUtil csvUtil;

    @Autowired
    MobileData mobileData;

    @When("[Subscription-UI-Mobile] user click semua favorit kamu")
    public void subscriptionUIMobileUserClickSemuaFavoritKamu() {
        favoritPageMobile.clickSemua();
    }

    @And("[Subscription-UI-Mobile] user choose bliblimart category")
    public void subscriptionUIMobileUserChooseBliblimartCategory() {
        favoritPageMobile.clickBlimart();
    }

    @And("[Subscription-UI-Mobile] user click langganan tab")
    public void subscriptionUIMobileUserClickLanggananTab() {
        micrositePageMobile.clickLanggananTab();
    }

    @Then("[Subscription-UI-Mobile] user should see subscription banner")
    public void subscriptionUIMobileUserShouldSeeSubscriptionBanner() {
        assertThat("no banner displayed", micrositePageMobile.getBannerSubs(),
                Matchers.equalTo(true));
    }

    @And("[Subscription-UI-Mobile] user should see cara berlangganan blimart section")
    public void subscriptionUIMobileUserShouldSeeCaraBerlanggananBlimartSection() {
        assertThat("how to subscribe section not displayed", micrositePageMobile.getHowToSubscribe(),
                Matchers.equalTo(true));
    }

    @And("[Subscription-UI-Mobile] user should see blimart product set")
    public void subscriptionUIMobileUserShouldSeeBlimartProductSet() {
        assertThat("blimart product set not displayed", micrositePageMobile.getBlimartProductSet(),
                Matchers.equalTo(true));
    }

    @When("[Subscription-UI-Mobile] user click term and condition link")
    public void subscriptionUIMobileUserClickTermAndConditionLink() {
        micrositePageMobile.clickTnCSubs();
    }

    @Then("[Subscription-UI-Mobile] user should see the popup term and condition")
    public void subscriptionUIMobileUserShouldSeeThePopupTermAndCondition() {
        assertThat("popup term n condition of subs not displayed", tnCSubsPageMobile.getTermnCondPopup(),
                Matchers.equalTo(true));
    }

    @When("[Subscription-UI-Mobile] user click close the popup term and condition")
    public void subscriptionUIMobileUserClickCloseThePopupTermAndCondition() {
        tnCSubsPageMobile.clickClosePopup();
    }

    @Then("[Subscription-UI-Mobile] user should not see the popup term and condition")
    public void subscriptionUIMobileUserShouldNotSeeThePopupTermAndCondition() {
        assertThat("popup doesnt close", micrositePageMobile.getBlimartProductSet(),
                Matchers.equalTo(true));
    }

    @When("[Subscription-UI-Mobile] user save the text as value with key to file csv {string}")
    public void subscriptionUIMobileUserSaveTheTextAsValueWithKeyToFileCsvFilename(String filename) {
        csvUtil.writeFile(MobileData.class, mobileData, filename);
    }

    @When("[Subscription-UI-Mobile] user get title of how to subscribe")
    public void subscriptionUIMobileUserGetTitleOfHowToSubscribe() {
        mobileData.setTitles(micrositePageMobile.getTitleHowToSubs());
    }
}
