package com.gdn.qa.future.steps.desktop;

import com.gdn.qa.future.BaseStepDefinition;
import com.gdn.qa.future.api.controller.ProductDetailController;
import com.gdn.qa.future.data.api.ProductDetailsApiData;
import com.gdn.qa.future.properties.ProductProperties;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.hamcrest.Matchers;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class GetProductDetailSteps extends BaseStepDefinition {
    @Autowired
    private ProductDetailsApiData productDetailsApiData;

    @Autowired
    private ProductProperties productProperties;

    @Autowired
    private ProductDetailController productDetailController;

    @When("[Subscription-API-Desktop] user send get product detail request")
    public void subscriptionAPIDesktopUserSendGetProductDetailRequest() {
        productDetailsApiData.setProductDetailsResponse(productDetailController
                .getProductDetailRequest());
    }

    @Then("[Subscription-API-Desktop] user should see product detail response with status code {int}")
    public void subscriptionAPIDesktopUserShouldSeeProductDetailResponseWithStatusCode(int code) {
        assertThat("get product details response failed", productDetailsApiData.getProductDetailsResponse().getCode(),
                equalTo(code));
    }

    @And("[Subscription-API-Desktop] user should see product detail response with status message {string}")
    public void subscriptionAPIDesktopUserShouldSeeProductDetailResponseWithStatusMessageOK(String statusMessage) {
        assertThat("get product details response failed", productDetailsApiData.getProductDetailsResponse().getStatus(),
                equalTo(statusMessage));
    }

    @And("[Subscription-API-Desktop] user should see product detail response with tags contain {string}")
    public void subscriptionAPIDesktopUserShouldSeeProductDetailResponseWithTagsContainSUBSCRIPTION(String tag) {
        assertThat("get product details response failed", productDetailsApiData.getProductDetailsResponse().getData().getTags(),
                Matchers.hasItemInArray(tag));
    }

    @Given("[Subscription-API-Desktop] user prepare data get product detail with {string}")
    public void subscriptionAPIDesktopUserPrepareDataGetProductDetailWithProductSku(String productSku) {
        productDetailsApiData.setProductSku(productProperties.getItemSku().get(productSku));
    }

    @And("[Subscription-API-Desktop] user prepare data get product detail with product code by {string}")
    public void subscriptionAPIDesktopUserPrepareDataGetProductDetailWithProductCodeByProductCode(String productCode) {
        if (productCode.equalsIgnoreCase("productCode")){
            productDetailsApiData.setProductCode(productProperties.getProductCode());
        }else {
            productDetailsApiData.setProductCode(productCode);
        }
    }
}
