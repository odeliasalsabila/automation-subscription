package com.gdn.qa.future.steps.mobile;

import com.gdn.qa.future.BaseStepDefinition;
import com.gdn.qa.future.data.ui.ProductData;
import com.gdn.qa.future.pages.mobile.MicrositePageMobile;
import com.gdn.qa.future.pages.mobile.OnBoardingMobilePage;
import com.gdn.qa.future.pages.mobile.ProductDetailPageMobile;
import com.gdn.qa.future.pages.mobile.ProductListPageMobile;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.hamcrest.Matchers;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;

public class OnboardingSubsMobileSteps extends BaseStepDefinition {
    @Autowired
    private MicrositePageMobile micrositePageMobile;

    @Autowired
    private ProductListPageMobile productListPageMobile;

    @Autowired
    private ProductDetailPageMobile productDetailPageMobile;

    @Autowired
    private OnBoardingMobilePage onBoardingMobilePage;

    @Autowired
    private ProductData productData;

    @And("[Subscription-UI-Mobile] user click lihat semua on microsite page")
    public void subscriptionUIMobileUserClickLihatSemuaOnMicrositePage() {
        micrositePageMobile.clickLihatSemuaOnProductSet();
    }

    @And("[Subscription-UI-Mobile] user click hide the oos product")
    public void subscriptionUIMobileUserClickHideTheOosProduct() {
        productListPageMobile.clickHideOosProduct();
    }

    @And("[Subscription-UI-Mobile] user click first product on product list page")
    public void subscriptionUIMobileUserClickFirstProductOnProductListPage() {
        productData.setProductName(productListPageMobile.getProductName());
        System.out.println(productData.getProductName());
        productListPageMobile.clickFirstProductBlimart();
    }

    @Then("[Subscription-UI-Mobile] user should see onboarding subscription if exist")
    public void subscriptionUIMobileUserShouldSeeOnboardingSubscription() {
        if (productDetailPageMobile.getOnboardingSubs()){
            assertThat("onboarding subscription not displayed", productDetailPageMobile.getOnboardingSubs(),
                    Matchers.equalTo(true));
        }
    }

    @And("[Subscription-UI-Mobile] user click filter on product list page")
    public void subscriptionUIMobileUserClickFilterOnProductListPage() {
        productListPageMobile.clickFilter();
    }

    @And("[Subscription-UI-Mobile] user click terapkan on filtering section")
    public void subscriptionUIMobileUserClickTerapkanOnFilteringSection() {
        productListPageMobile.clickTerapkan();
    }

    @And("[Subscription-UI-Mobile] user click mantap on top rated onborading if exist")
    public void subscriptionUIMobileUserClickMantapOnTopRatedOnboradingIfExist() {
        if (onBoardingMobilePage.getMantapButtonOnBoarding()){
            productListPageMobile.clickMantap();
        }
    }

    @And("[Subscription-UI-Mobile] user should see oke on onboarding subscription if exist")
    public void subscriptionUIMobileUserShouldSeeOkeOnOnboardingSubscriptionIfExist() {
        if (productDetailPageMobile.getOkeOnOnboardingSubs()){
            assertThat("onboarding subscription not displayed", productDetailPageMobile.getOkeOnOnboardingSubs(),
                    Matchers.equalTo(true));
        }
    }
}
