package com.gdn.qa.future.steps.desktop;

import com.gdn.qa.future.BaseStepDefinition;
import com.gdn.qa.future.api.controller.UserController;
import com.gdn.qa.future.data.ui.UserData;
import com.gdn.qa.future.data.api.UserLoginApiData;
import com.gdn.qa.future.pages.desktop.BlibliHomePage;
import com.gdn.qa.future.pages.desktop.LoginDesktopFormPage;
import com.gdn.qa.future.pages.desktop.ProfilePage;
import com.gdn.qa.future.pages.desktop.TickerPage;
import com.gdn.qa.future.properties.DefaultProperties;
import com.gdn.qa.future.properties.ServiceProperties;
import com.gdn.qa.future.properties.UserProperties;
import com.gdn.qa.future.utils.csvUtil;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.hamcrest.Matchers;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class LoginDesktopSteps extends BaseStepDefinition {
    @Autowired
    private BlibliHomePage blibliHomepage;

    @Autowired
    private LoginDesktopFormPage loginDesktopFormPage;

    @Autowired
    private ProfilePage profilePage;

    @Autowired
    private UserData userData;

    @Autowired
    private UserController userController;

    @Autowired
    private UserProperties userProperties;

    @Autowired
    private TickerPage tickerPage;

    @Autowired
    private UserLoginApiData userLoginApiData;

    @Autowired
    private ServiceProperties serviceProperties;

    @Autowired
    private DefaultProperties defaultProperties;

    @Given("[Subscription-UI-Desktop] user at Bliblicom homepage")
    public void subscriptionUIDesktopUserAtBliblicomHomepage() {
        blibliHomepage.open();
        blibliHomepage.addCookie(defaultProperties.getCookies());
        blibliHomepage.closeIframe();
    }

    @When("[Subscription-UI-Desktop] user click link login")
    public void subscriptionUIDesktopUserClickLinkLogin() {
        loginDesktopFormPage.clickLoginButton();
    }

    @And("[Subscription-UI-Desktop] user input email with value {string}")
    public void subscriptionUIDesktopUserInputEmailWithValueUserEmail(String emailTxt) {
        if (emailTxt.equalsIgnoreCase("userEmail")) {
            userData.setEmailUser(userProperties.getEmail());
        }else {
            userData.setEmailUser(emailTxt);
        }
        loginDesktopFormPage.typeEmail(userData.getEmailUser());
    }

    @And("[Subscription-UI-Desktop] user input password with value {string}")
    public void subscriptionUIDesktopUserInputPasswordWithValueUserPassword(String passwordTxt) {
        if (passwordTxt.equalsIgnoreCase("userPassword")){
            userData.setPasswordUser(userProperties.getPassword());
        }
        else {
            userData.setPasswordUser(passwordTxt);
        }
        loginDesktopFormPage.typePassword(userData.getPasswordUser());
    }

    @And("[Subscription-UI-Desktop] user click button masuk")
    public void subscriptionUIDesktopUserClickButtonMasuk() {
        loginDesktopFormPage.clickLoginButtonForm();
    }

    @Then("[Subscription-UI-Desktop] user should be at the homepage")
    public void subscriptionUIDesktopUserShouldBeAtTheHomepage() {
        assertThat("user not in the blibli homepage", blibliHomepage.viewCaraouselHomepage(),
                Matchers.equalTo(true));
    }

    @When("[Subscription-UI-Desktop] user click account")
    public void subscriptionUIDesktopUserClickAccount() {
        profilePage.open();
    }

    @And("[Subscription-UI-Desktop] user close the pop up")
    public void subscriptionUIDesktopUserCloseThePopUp() {
        blibliHomepage.closePopUp();
    }

    @Then("[Subscription-UI-Desktop] user should see email as previously entered on info data account")
    public void subscriptionUIDesktopUserShouldSeeEmailAsPreviouslyEnteredOnInfoDataAccount() {
        assertThat("email doesnt match", profilePage.getEmailLoggedIn(),
                Matchers.equalTo(userData.getEmailUser()));
    }

    @And("[Subscription-UI-Desktop] user click email validation button")
    public void subscriptionUIDesktopUserClickEmailValidationButton() {
        loginDesktopFormPage.clickEmailValidationBtn();
    }

    @And("[Subscription-UI-Desktop] user fill the code of email validation")
    public void subscriptionUIDesktopUserFillTheCodeOfEmailValidation() {
        if (loginDesktopFormPage.getVerificationCodeEmailForm()){
            loginDesktopFormPage.verificationCodeEmailForm(userData.getVerificationCode());
        }
    }

    @When("[Subscription-UI-Desktop] user click nanti saja on access location")
    public void subscriptionUIDesktopUserClickNantiSajaOnAccessLocation() {
        blibliHomepage.clickNantiSajaOnAccessLocation();
    }

    @And("[Subscription-UI-Desktop] user click verifikasi button of email verification form")
    public void subscriptionUIDesktopUserClickVerifikasiButtonOfEmailVerificationForm() {
        if (loginDesktopFormPage.verificationButtonEmailIsClickable()){
            loginDesktopFormPage.clickVerifikasiButton();
        }
    }

    @Given("[Subscription-API-Desktop] user get cookies")
    public void subscriptionAPIDesktopUserGetCookies() {
        userData.setLoginCookies(profilePage.getCookies());
    }

    @And("[Subscription-API-Desktop] user prepare data get user has logged in with cookies")
    public void subscriptionAPIDesktopUserPrepareDataGetUserHasLoggedInWithCookies() {
        userLoginApiData.setCookies(userData.getLoginCookies());
    }

    @When("[Subscription-API-Desktop] user send get user has logged in request")
    public void subscriptionAPIDesktopUserSendGetUserHasLoggedInRequest() {
        userLoginApiData.setLoginResponse(userController.getUserHasloggedIn());
    }

    @Then("[Subscription-API-Desktop] user should see get user has logged in response with status code {int}")
    public void subscriptionAPIDesktopUserShouldSeeGetUserHasLoggedInResponseWithStatusCode(int code) {
        assertThat("login gagal", userLoginApiData.getLoginResponse().getCode(), equalTo(code));
    }

    @And("[Subscription-API-Desktop] user should see get user has logged in response with status message {string}")
    public void subscriptionAPIDesktopUserShouldSeeGetUserHasLoggedInResponseWithStatusMessageOK(String statusMessage) {
        assertThat("login gagal", userLoginApiData.getLoginResponse().getStatus(), equalTo(statusMessage));
    }

    @And("[Subscription-API-Desktop] user should see get user has logged in response with email {string}")
    public void subscriptionAPIDesktopUserShouldSeeGetUserHasLoggedInResponseWithEmailValidEmail(String emailTxt) {
        if (emailTxt.equalsIgnoreCase("userEmail")){
            assertThat("login gagal", userLoginApiData.getLoginResponse().getData().getEmail(),
                    equalTo(userData.getEmailUser()));
        }
    }

    @And("[Subscription-UI-Desktop] user click nanti saja on verification page")
    public void subscriptionUIDesktopUserClickNantiSajaOnVerificationPage() {
        loginDesktopFormPage.clickNantiSaja();
    }

    @And("[Subscription-UI-Desktop] user close the iframe homepage")
    public void subscriptionUIDesktopUserCloseThePopUpHomepage() {
        blibliHomepage.closeIframe();
    }

    @Then("[Subscription-UI-Desktop] user should see the login button is disabled")
    public void subscriptionUIDesktopUserShouldSeeTheLoginButtonIsDisabled() {
        assertThat("login button is enabled", loginDesktopFormPage.getLoginButton(),
                Matchers.equalTo(true));
    }

    @Then("[Subscription-UI-Desktop] user should see error's ticker")
    public void subscriptionUIDesktopUserShouldSeeErrorSTicker() {
        assertThat("error's ticker doesn't show", tickerPage.getTickerError(),
                Matchers.equalTo(true));
    }

    @And("[Subscription-UI-Desktop] user should see {string} for {string}")
    public void subscriptionUIDesktopUserShouldSeeErrorMessageForNegativeScenario(String errorMessage, String scenario) {
        assertThat("error message doesnt match for scenario"+scenario, tickerPage.getErrorMessageLogin(),
                Matchers.equalToIgnoringCase(errorMessage));
    }

    @And("[Subscription-UI-Desktop] the session not expired")
    public void subscriptionUIDesktopVerifyTheSessionNotExpired() {
        if (loginDesktopFormPage.getSessionTextWarning()) {
            loginDesktopFormPage.closeVerifikasiEmailForm();
            loginDesktopFormPage.clickBatalkanConfirmation();
            loginDesktopFormPage.clickLoginButtonForm();
            loginDesktopFormPage.clickEmailValidationBtn();
        }
    }
}
