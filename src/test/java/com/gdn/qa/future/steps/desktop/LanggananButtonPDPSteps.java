package com.gdn.qa.future.steps.desktop;

import com.gdn.qa.future.BaseStepDefinition;
import com.gdn.qa.future.data.ui.ProductData;
import com.gdn.qa.future.pages.desktop.LanggananPage;
import com.gdn.qa.future.pages.desktop.OnBoardingDesktopPage;
import com.gdn.qa.future.pages.desktop.ProductDetailPage;
import com.gdn.qa.future.pages.desktop.ProductListPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.hamcrest.Matchers;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;

public class LanggananButtonPDPSteps extends BaseStepDefinition {
    @Autowired
    ProductDetailPage productDetailPage;

    @Autowired
    LanggananPage langgananPage;

    @Autowired
    ProductData productData;

    @Autowired
    ProductListPage productListPage;

    @Autowired
    OnBoardingDesktopPage onBoardingDesktopPage;

    @Then("[Subscription-UI-Desktop] user should redirect to the PDP according to the product selected")
    public void subscriptionUIDesktopuserShouldRedirectToThePDPAccordingToTheProductSelected() {
        assertThat("Between PDP and product selected doesnt match", productDetailPage.getProductName(),
                Matchers.equalToIgnoringCase(productData.getProductName()));
    }

    @And("[Subscription-UI-Desktop] user click lihat semua on microsite page")
    public void subscriptionUIDesktopuserClickLihatSemuaOnMicrositePage() {
        langgananPage.clickLihatSemua();
    }

    @And("[Subscription-UI-Desktop] user click first product on product list page")
    public void subscriptionUIDesktopuserClickFirstProductOnProductListPage() {
        productData.setProductName(productListPage.getProductName());
        productListPage.clickProductSubscription();
    }

    @And("[Subscription-UI-Desktop] user click hide the oos product")
    public void subscriptionUIDesktopuserClickHideTheOosProduct() {
        productListPage.clickHideOosFilter();
    }

    @And("[Subscription-UI-Desktop] user should see that the subscription button is clickable")
    public void subscriptionUIDesktopuserShouldSeeThatTheSubscriptionButtonIsClickable() {
        assertThat("subscription button is disable", productDetailPage.getSubsButton(),
                Matchers.equalTo(true));
    }

    @And("[Subscription-UI-Desktop] user click mantap on top rated onborading")
    public void subscriptionUIDesktopuserClickMantapOnTopRatedOnborading() {
        onBoardingDesktopPage.clickMantapTopRatedOnborading();
    }

    @And("[Subscription-UI-Desktop] user click oke on subscription onboarding if exist")
    public void subscriptionUIDesktopuserClickOkeOnSubscriptionOnboardingIfExist() {
        if (onBoardingDesktopPage.getOkeOnSubsOnboarding()){
            onBoardingDesktopPage.clickOkeOnSubsOnboarding();
        }
    }

    @Then("[Subscription-UI-Desktop] user should see onboarding subscription if exist")
    public void subscriptionUIDesktopUserShouldSeeOnboardingSubscriptionIfExist() {
        if (onBoardingDesktopPage.getOnboardingSubs()){
            assertThat("onboarding doesnt show", onBoardingDesktopPage.getOnboardingSubs(),
                    Matchers.equalTo(true));
        }
    }
}
