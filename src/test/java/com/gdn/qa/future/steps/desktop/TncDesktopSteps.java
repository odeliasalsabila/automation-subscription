package com.gdn.qa.future.steps.desktop;

import com.gdn.qa.future.BaseStepDefinition;
import com.gdn.qa.future.pages.desktop.TncSubsDesktopPage;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.hamcrest.Matchers;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;

public class TncDesktopSteps extends BaseStepDefinition {
    @Autowired
    private TncSubsDesktopPage tncSubsDesktopPage;

    @When("[Subscription-UI-Desktop] user click term and condition link")
    public void subscriptionUIDesktopuserClickTermAndConditionLink() {
        tncSubsDesktopPage.clickTermAndConditionLink();
    }

    @Then("[Subscription-UI-Desktop] user should see the popup term and condition")
    public void subscriptionUIDesktopuserShouldSeeThePopupTermAndCondition() {
        assertThat("Term and condition popup did not displayed", tncSubsDesktopPage.viewTncPopup(),
                Matchers.equalTo(true));
    }

    @When("[Subscription-UI-Desktop] user click close the popup term and condition")
    public void subscriptionUIDesktopuserClickCloseThePopupTermAndCondition() {
        tncSubsDesktopPage.clickCloseButton();
    }

    @Then("[Subscription-UI-Desktop] user should not see the popup term and condition")
    public void subscriptionUIDesktopuserShouldNotSeeThePopupTermAndCondition() {
        assertThat("Term and condition popup still displayed", tncSubsDesktopPage.tncPopupClosed(),
                Matchers.equalTo(true));
    }
}
