package com.gdn.qa.future.steps.desktop;

import com.gdn.qa.future.BaseStepDefinition;
import com.gdn.qa.future.api.controller.SubscriptionCartController;
import com.gdn.qa.future.data.api.SubscriptionCartApiData;
import com.gdn.qa.future.data.ui.ProductData;
import com.gdn.qa.future.pages.desktop.*;
import com.gdn.qa.future.pages.mobile.TnCSubsPageMobile;
import com.gdn.qa.future.properties.ProductProperties;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.hamcrest.Matchers;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class SubscriptionSteps extends BaseStepDefinition {
    @Autowired
    private ProductDetailPage productDetailPage;

    @Autowired
    private LoginDesktopFormPage loginDesktopFormPage;

    @Autowired
    private ProductData productData;

    @Autowired
    private PopupPage popupPage;

    @Autowired
    private SubscriptionCartPage subscriptionCartPage;

    @Autowired
    private BlibliHomePage blibliHomePage;

    @Autowired
    private ProductProperties productProperties;

    @Autowired
    private SearchDesktopPage searchDesktopPage;

    @Autowired
    private OnBoardingDesktopPage onBoardingDesktopPage;

    @Autowired
    private SubscriptionCartApiData subscriptionCartApiData;

    @Autowired
    private SubscriptionCartController subscriptionCartController;

    @Autowired
    private TnCSubsPageMobile tnCSubsPageMobile;

    @When("[Subscription-UI-Desktop] user click subscription button")
    public void subscriptionUIDesktopUserClickSubscriptionButton() {
        productDetailPage.clickSubsButton();
    }

    @Then("[Subscription-UI-Desktop] user should see login form")
    public void subscriptionUIDesktopUserShouldSeeLoginForm() {
        assertThat("form login doesnt show", loginDesktopFormPage.getFormLogin(),
                Matchers.equalTo(true));
    }

    @And("[Subscription-UI-Desktop] user click siap on subscription onboarding if exist")
    public void subscriptionUIDesktopUserClickSiapOnSubscriptionOnboardingIfExist() {
        if (onBoardingDesktopPage.getSiapOnBoardingSubs()){
            onBoardingDesktopPage.clickSiapOnOnboardingSubs();
        }
    }

    @Then("[Subscription-UI-Desktop] user should see the subscription success's popup")
    public void subscriptionUIDesktopUserShouldSeeTheSubscriptionSuccessSPopup() {
        assertThat("popup of subscription succes doesn't show", popupPage.getSuccesfulSubscribe(),
                Matchers.equalTo(true));
    }

    @And("[Subscription-UI-Desktop] user should see lihat langganan button is clickable")
    public void subscriptionUIDesktopUserShouldSeeLihatLanggananButtonIsClickable() {
        assertThat("lihat langganan button not show/not clickable", popupPage.getLihatLanggananButton(),
                Matchers.equalTo(true));
    }

    @When("[Subscription-UI-Desktop] user click delete icon on product subscription")
    public void subscriptionUIDesktopUserClickDeleteIconOnProductSubscription() {
        subscriptionCartPage.clickTrashIconOnProduct(productData.getProductName());
    }

    @And("[Subscription-UI-Desktop] user click delete button for clarification")
    public void subscriptionUIDesktopUserClickDeleteButtonForClarification() {
        subscriptionCartPage.clickDeleteClarification();
    }

    @When("[Subscription-UI-Desktop] user search product subscription using {string}")
    public void subscriptionUIDesktopUserSearchProductSubscriptionUsingProductSku(String productSku) {
        productData.setProductSku(productProperties.getProductSku().get(productSku));
        searchDesktopPage.searchProduct(productData.getProductSku());
    }

    @And("[Subscription-UI-Desktop] user click icon search")
    public void subscriptionUIDesktopUserClickIconSearch() {
        searchDesktopPage.clickSearchIcon();
    }

    @And("[Subscription-UI-Desktop] user click siap on two our delivery onborading")
    public void subscriptionUIDesktopUserClickSiapOnTwoOurDeliveryOnborading() {
        onBoardingDesktopPage.clickSiapOnTwoDelivery();
    }

    @When("[Subscription-UI-Desktop] user click buat langganan baru")
    public void subscriptionUIDesktopUserClickBuatLanggananBaru() {
        popupPage.clickBuatLanggananBaru();
    }

    @Then("[Subscription-UI-Desktop] user should see the correct product subscription according to {string}")
    public void subscriptionUIDesktopUserShouldSeeTheCorrectProductSubscriptionAccordingToBundles(String bundles) {
        assertThat("the correct product subscription is not visible", subscriptionCartPage.getListProductName(bundles),
                Matchers.hasItems(productData.getProductName()));
    }

    @Then("[Subscription-UI-Desktop] user should not see those product subscription again according to {string}")
    public void subscriptionUIDesktopUserShouldNotSeeThoseProductSubscriptionAgainAccordingToBundles(String bundles) {
        assertThat("the product still displayed", subscriptionCartPage.getListProductName(bundles),
                not(hasItem(productData.getProductName())));
    }

    @When("[Subscription-API-Desktop] user send subscription cart request")
    public void subscriptionAPIDesktopUserSendSubscriptionCartRequest() {
        subscriptionCartApiData.setSubscriptionCartResponse(subscriptionCartController
                .addSubscriptionToCartResponse());
    }

    @Then("[Subscription-API-Desktop] user should see subscription cart response with status code {int}")
    public void subscriptionAPIDesktopUserShouldSeeSubscriptionCartResponseWithStatusCode(int code) {
        assertThat("add product subscription to cart failed",
                subscriptionCartApiData.getSubscriptionCartResponse().getCode(), equalTo(code));
    }

    @And("[Subscription-API-Desktop] user should see subscription cart response with status message {string}")
    public void subscriptionAPIDesktopUserShouldSeeSubscriptionCartResponseWithStatusMessageOK(String status) {
        assertThat("add product subscription to cart failed",
                subscriptionCartApiData.getSubscriptionCartResponse().getStatus(), equalTo(status));
    }

    @And("[Subscription-API-Desktop] user should see subscription cart response with name as the previous product added")
    public void subscriptionAPIDesktopUserShouldSeeSubscriptionCartResponseWithNameAsThePreviousProductAdded() {
        List<String> name = new ArrayList<>();
        for (int i=0; i< subscriptionCartApiData.getSubscriptionCartResponse().getData().getItems().size(); i++){
            name.add(subscriptionCartApiData.getSubscriptionCartResponse().getData().getItems().get(i).getName());
        }
        assertThat("name product doesnt match/not displayed", name, Matchers.hasItem((productData.getProductName())));
    }

    @And("[Subscription-API-Desktop] user should see subscription cart response with item sku as the previous product added")
    public void subscriptionAPIDesktopUserShouldSeeSubscriptionCartResponseWithItemSkuAsThePreviousProductAdded() {
        List<String> itemSku = new ArrayList<>();
        for (int i=0; i< subscriptionCartApiData.getSubscriptionCartResponse().getData().getItems().size(); i++){
            itemSku.add(subscriptionCartApiData.getSubscriptionCartResponse().getData().getItems().get(i).getItemSku());
        }

        assertThat("name product doesnt match/not displayed", itemSku, Matchers.hasItem(subscriptionCartApiData.getItemSku()));
    }

    @And("[Subscription-UI-Mobile] user click swipe to term and condition link")
    public void subscriptionUIMobileUserClickSwipeToTermAndConditionLink() {
        tnCSubsPageMobile.getTermnCondLink();
    }
}
