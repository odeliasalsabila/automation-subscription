package com.gdn.qa.future.steps.desktop;

import com.gdn.qa.future.BaseStepDefinition;
import com.gdn.qa.future.api.controller.SubscriptionSectionController;
import com.gdn.qa.future.api.response.getSubsSection.SubsSectionParameter;
import com.gdn.qa.future.data.api.HowToSubsApiData;
import com.gdn.qa.future.data.comparisonData.APIData;
import com.gdn.qa.future.data.comparisonData.UiData;
import com.gdn.qa.future.pages.desktop.BlibliHomePage;
import com.gdn.qa.future.pages.desktop.FavoritesPage;
import com.gdn.qa.future.pages.desktop.LanggananPage;
import com.gdn.qa.future.utils.csvUtil;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.hamcrest.Matchers;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class LanggananTabDesktopSteps extends BaseStepDefinition {
    @Autowired
    private BlibliHomePage blibliHomePage;

    @Autowired
    private FavoritesPage favoritesPage;

    @Autowired
    private LanggananPage langgananPage;

    @Autowired
    private csvUtil csvUtil;

    @Autowired
    private HowToSubsApiData howToSubsApiData;

    @Autowired
    private SubscriptionSectionController subscriptionSectionController;

    @Autowired
    private APIData apiData;

    @Autowired
    private UiData uiData;

    @When("[Subscription-UI-Desktop] user click semua favorit kamu")
    public void subscriptionUIDesktopuserClickSemuaFavoritKamu() {
        blibliHomePage.clickSemuaFavoritnyaKamu();
    }

    @And("[Subscription-UI-Desktop] user choose bliblimart category")
    public void subscriptionUIDesktopuserChooseBliblimartCategory() {
        favoritesPage.clickBliblimartCategory();
    }

    @And("[Subscription-UI-Desktop] user click langganan tab")
    public void subscriptionUIDesktopuserClickLanggananTab() {
        langgananPage.clickLanggananTab();
    }

    @Then("[Subscription-UI-Desktop] user should see subscription banner")
    public void subscriptionUIDesktopuserShouldSeeSubscriptionBanner() {
        assertThat("there's no subscription banner", langgananPage.viewBanner(),
                Matchers.equalTo(true));
    }

    @And("[Subscription-UI-Desktop] user should see cara berlangganan blimart section")
    public void subscriptionUIDesktopuserShouldSeeCaraBerlanggananBlimartSection() {
        assertThat("there's no cara berlangganan section", langgananPage.viewHowToSubsSection(),
                Matchers.equalTo(true));
    }

    @And("[Subscription-UI-Desktop] user should see blimart product set")
    public void subscriptionUIDesktopuserShouldSeeBlimartProductSet() {
        assertThat("there are no product set displayed", langgananPage.viewProductSet(),
                Matchers.equalTo(true));
    }

    @When("[Subscription-UI-Desktop] user get text for how to subscribe")
    public void subscriptionUIDesktopUserGetTextForHowToSubscribe() {
        uiData.setTitles(langgananPage.getHowToSubsText());
    }

    @And("[Subscription-API-Desktop] user send get blm subs section request")
    public void subscriptionAPIDesktopUserSendGetBlmSubsSectionRequest() {
        howToSubsApiData.setGetSubsSectionResponse(subscriptionSectionController.getSubsSectionResponse());
    }

    @Then("[Subscription-API-Desktop] user should see get blm subs section response with status code {int}")
    public void subscriptionAPIDesktopUserShouldSeeGetBlmSubsSectionResponseWithStatusCode(int code) {
        assertThat("blm section failed", howToSubsApiData.getGetSubsSectionResponse().getCode(), equalTo(code));
    }

    @And("[Subscription-API-Desktop] user should see get blm subs section response with status message {string}")
    public void subscriptionAPIDesktopUserShouldSeeGetBlmSubsSectionResponseWithStatusMessageOK(String status) {
        assertThat("blm section failed", howToSubsApiData.getGetSubsSectionResponse().getStatus(), equalTo(status));
    }

    @When("[Subscription-API-Desktop] user save the title from blm subs section response")
    public void subscriptionAPIDesktopUserSaveTheTitleFromBlmSubsSectionResponse() {
        List<SubsSectionParameter> subsSectionParameters = howToSubsApiData.getGetSubsSectionResponse().getData().get(0)
                .getBlocks().get(3).getComponents().get(0).getParameters();
        List<String> titles = new ArrayList<>();
        for (SubsSectionParameter subsSectionParameter : subsSectionParameters) {
            if (subsSectionParameter.getType().equalsIgnoreCase("image")) {
                titles.add(subsSectionParameter.getTitle());
            }
        }
        apiData.setTitles(titles);
    }

    @And("[Subscription-API-Desktop] user save the text as value with key to file csv {string}")
    public void subscriptionAPIDesktopUserSaveTheTextAsValueWithKeyToFileCsvResultAPI(String filename) {
        csvUtil.writeFile(APIData.class, apiData, filename);
    }

    @And("[Subscription-API-Desktop] user save the type from blm subs section response")
    public void subscriptionAPIDesktopUserSaveTheTypeFromBlmSubsSectionResponse() {
        List<SubsSectionParameter> subsSectionParameters = howToSubsApiData.getGetSubsSectionResponse().getData().get(0)
                .getBlocks().get(3).getComponents().get(0).getParameters();
        List<String> types = new ArrayList<>();
        for (SubsSectionParameter subsSectionParameter : subsSectionParameters) {
            if (subsSectionParameter.getType().equalsIgnoreCase("image")) {
                types.add(subsSectionParameter.getType());
            }
        }
        apiData.setTypes(types);
    }

    @And("[Subscription-UI-Desktop] user save the text as value with key to file csv {string}")
    public void subscriptionUIDesktopUserSaveTheTextAsValueWithKeyToFileCsvFilename(String filename) {
        csvUtil.writeFile(UiData.class, uiData, filename);
    }
}
