package com.gdn.qa.future.steps.desktop;

import com.gdn.qa.future.BaseStepDefinition;
import com.gdn.qa.future.api.controller.SubscriptionProductController;
import com.gdn.qa.future.data.api.SubscriptionProductApiData;
import com.gdn.qa.future.data.ui.ProductData;
import com.gdn.qa.future.properties.ProductProperties;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.hamcrest.Matchers;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class SubscriptionProductSteps extends BaseStepDefinition {
    @Autowired
    ProductProperties productProperties;

    @Autowired
    SubscriptionProductApiData subscriptionProductApiData;

    @Autowired
    SubscriptionProductController subscriptionProductController;

    @Autowired
    ProductData productData;

    @Given("[Subscription-API-Desktop] user prepare data get subscription products with item sku by {string}")
    public void subscriptionAPIDesktopUserPrepareDataGetSubscriptionProductsWithItemSkuBySubscription(String itemSku) {
        subscriptionProductApiData.setItemSku(productProperties.getItemSku().get(itemSku));
    }

    @When("[Subscription-API-Desktop] user send get subscription products request")
    public void subscriptionAPIDesktopUserSendGetSubscriptionProductsRequest() {
        subscriptionProductApiData.setSubscriptionProductResponse(subscriptionProductController
                .getSubscriptionProductResponse());
    }

    @Then("[Subscription-API-Desktop] user should see get subscription product response with status code {int}")
    public void subscriptionAPIDesktopUserShouldSeeGetSubscriptionProductResponseWithStatusCode(int code) {
        assertThat("get subscription product failed to response", subscriptionProductApiData
                        .getSubscriptionProductResponse().getCode(), equalTo(code));
    }

    @And("[Subscription-API-Desktop] user should see get subscription product response with status message {string}")
    public void subscriptionAPIDesktopUserShouldSeeGetSubscriptionProductResponseWithStatusMessageOK(String status) {
        assertThat("get subscription product failed to response", subscriptionProductApiData
                .getSubscriptionProductResponse().getStatus(), equalTo(status));
    }

    @And("[Subscription-API-Desktop] user should see get subscription product respone with item sku as the previous product added")
    public void subscriptionAPIDesktopUserShouldSeeGetSubscriptionProductResponeWithItemSkuAsThePreviousProductAdded() {
        assertThat("get subscription product failed to response", subscriptionProductApiData
                .getSubscriptionProductResponse().getData().getItemSku(), equalTo(subscriptionProductApiData.getItemSku()));
    }

    @And("[Subscription-API-Desktop] user should see get subscription product respone with name as the previous product added")
    public void subscriptionAPIDesktopUserShouldSeeGetSubscriptionProductResponeWithNameAsThePreviousProductAdded() {
        assertThat("get subscription product failed to response", subscriptionProductApiData
                .getSubscriptionProductResponse().getData().getName().trim(), Matchers.equalToIgnoringCase(productData.getProductName()));
    }
}
