package com.gdn.qa.future;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features="src/test/resources/features/",
        stepNotifications = true,
        tags = "@Subscription"
)
public class CucumberRunner {}
