@CompareDataFeature
Feature: compare data

  Scenario: compare data from API, UI, MOBILE
    Given [Subscription-API-Desktop] user get value from file 'resultAPI'
    When [Subscription-UI-Desktop] user get value from file 'resultUI'
    And [Subscription-UI-Mobile] user get value from file 'resultMobile'
    Then value should each other