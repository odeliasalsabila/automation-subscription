@HowToSubsMobilePlusScoreFeature
Feature: How To Subscribe Feature

  Scenario: Get text how to subscribe from Mobile
    Given [Subscription-UI-Mobile] user open blibli app
    When [Subscription-UI-Mobile] user click semua favorit kamu
    And [Subscription-UI-Mobile] user choose bliblimart category
    And [Subscription-UI-Mobile] user click langganan tab
    Then [Subscription-UI-Mobile] user should see subscription banner
    And [Subscription-UI-Mobile] user should see cara berlangganan blimart section
    When [Subscription-UI-Mobile] user get title of how to subscribe
    And [Subscription-UI-Mobile] user save the text as value with key to file csv 'resultMobile'