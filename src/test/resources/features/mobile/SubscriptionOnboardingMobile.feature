@SubscriptionOnboardingMobileFeature @Subscription @Mobile
  Feature: Subscription Onboarding Feature

    @Positive
    Scenario: Verify onboarding subscription on available blimart product in mobile
      Given [Subscription-UI-Mobile] user open blibli app
      When [Subscription-UI-Mobile] user click semua favorit kamu
      And [Subscription-UI-Mobile] user choose bliblimart category
      And [Subscription-UI-Mobile] user click langganan tab
      And [Subscription-UI-Mobile] user click swipe to term and condition link
      And [Subscription-UI-Mobile] user click lihat semua on microsite page
      And [Subscription-UI-Mobile] user click mantap on top rated onborading if exist
      And [Subscription-UI-Mobile] user click first product on product list page
      Then [Subscription-UI-Mobile] user should see onboarding subscription if exist
