@SubscribeMobileFeature @Subscription @Mobile
Feature: Subscribe mobile feature

  @Positive
  Scenario Outline: As a customer I want to subscribe new product using mobile app
    Given [Subscription-UI-Mobile] user open blibli app
    When [Subscription-UI-Mobile] user click semua favorit kamu
    And [Subscription-UI-Mobile] user choose bliblimart category
    And [Subscription-UI-Mobile] user click langganan tab
    And [Subscription-UI-Mobile] user click swipe to term and condition link
    And [Subscription-UI-Mobile] user click lihat semua on microsite page
    And [Subscription-UI-Mobile] user click mantap on top rated onborading if exist
    And [Subscription-UI-Mobile] user click first product on product list page
    Then [Subscription-UI-Mobile] user should see onboarding subscription if exist
    And [Subscription-UI-Mobile] user should see oke on onboarding subscription if exist
    When [Subscription-UI-Mobile] user click oke on onboarding subscription if exist
    Then [Subscription-UI-Mobile] user should see berlangganan button is clickable
    When [Subscription-UI-Mobile] user click subscription button
    And [Subscription-UI-Mobile] user type 'userEmail' and 'userPassword'
    And [Subscription-UI-Mobile] user click login button form
    And [Subscription-UI-Mobile] user click nanti saja on verification page
    And [Subscription-UI-Mobile] user click siap on subscription onboarding if exist
    And [Subscription-UI-Mobile] user choose paket langganan with '<values>'
    And [Subscription-UI-Mobile] user fill quantity with '<quantity>'
    And [Subscription-UI-Mobile] user click tambah ke langganan button
    Then [Subscription-UI-Mobile] user should see the subscription success's popup
    And [Subscription-UI-Mobile] user should see lihat langganan button is clickable
    Examples:
      | values   | quantity |
      | 5 minggu | 2        |