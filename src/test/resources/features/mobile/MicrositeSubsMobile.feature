@MicrositeSubsMobileFeature @Subscription @Mobile
Feature: Microsite Subscription Mobile Feature

  @Positive @SubscriptionBanner
  Scenario: As a customer i want to access langganan page mobile
    Given [Subscription-UI-Mobile] user open blibli app
    When [Subscription-UI-Mobile] user click semua favorit kamu
    And [Subscription-UI-Mobile] user choose bliblimart category
    And [Subscription-UI-Mobile] user click langganan tab
    Then [Subscription-UI-Mobile] user should see subscription banner
    And [Subscription-UI-Mobile] user should see cara berlangganan blimart section
    And [Subscription-UI-Mobile] user should see blimart product set

  @Positive @TermAndCondition
  Scenario: Succesfully open the popup of subscription term and condition in mobile
    Given [Subscription-UI-Mobile] user open blibli app
    When [Subscription-UI-Mobile] user click semua favorit kamu
    And [Subscription-UI-Mobile] user choose bliblimart category
    And [Subscription-UI-Mobile] user click langganan tab
    Then [Subscription-UI-Mobile] user should see cara berlangganan blimart section
    When [Subscription-UI-Mobile] user click term and condition link
    Then [Subscription-UI-Mobile] user should see the popup term and condition
    When [Subscription-UI-Mobile] user click close the popup term and condition
    Then [Subscription-UI-Mobile] user should not see the popup term and condition