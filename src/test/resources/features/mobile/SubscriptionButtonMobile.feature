@SubscriptionButtonFeature @Subscription @Mobile
  Feature: Subscription Button Feature

    @Positive
    Scenario: Verify subscription button is clickable for subscription product available in mobile
      Given [Subscription-UI-Mobile] user open blibli app
      When [Subscription-UI-Mobile] user click semua favorit kamu
      And [Subscription-UI-Mobile] user choose bliblimart category
      And [Subscription-UI-Mobile] user click langganan tab
      And [Subscription-UI-Mobile] user click swipe to term and condition link
      And [Subscription-UI-Mobile] user click lihat semua on microsite page
      And [Subscription-UI-Mobile] user click mantap on top rated onborading if exist
      And [Subscription-UI-Mobile] user click first product on product list page
      Then [Subscription-UI-Mobile] user should see onboarding subscription if exist
      And [Subscription-UI-Mobile] user should see oke on onboarding subscription if exist
      When [Subscription-UI-Mobile] user click oke on onboarding subscription if exist
      Then [Subscription-UI-Mobile] user should see berlangganan button is clickable