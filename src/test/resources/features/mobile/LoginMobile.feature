@LoginMobileFeature @Subscription @Mobile
Feature: Login into Blibli desktop feature

  @Positive
  Scenario Outline: Successfully logged into Blibli mobile using registered account
    Given [Subscription-UI-Mobile] user open blibli app
    When [Subscription-UI-Mobile] user click masuk
    And [Subscription-UI-Mobile] user type '<username>' and '<password>'
    And [Subscription-UI-Mobile] user click login button form
    And [Subscription-UI-Mobile] user click nanti saja on verification page
    And [Subscription-UI-Mobile] user click akun
    And [Subscription-UI-Mobile] user click user info
    Then [Subscription-UI-Mobile] validate user is already login as previous email filled in
    Examples:
      | username  | password     |
      | userEmail | userPassword |

  @Negative
  Scenario Outline: failed to login into Blibli mobile with scenario '<negative scenario>'
    Given [Subscription-UI-Mobile] user open blibli app
    When [Subscription-UI-Mobile] user click masuk
    And [Subscription-UI-Mobile] user type '<username>' and '<password>'
    And [Subscription-UI-Mobile] user click login button form
    Then [Subscription-UI-Mobile] user should see '<errorMessage>' for '<negative scenario>'
    Examples:
      | username  | password     | errorMessage                        | negative scenario     |
      | userEmail | sdfghjk      | Yakin itu benar? Coba diingat lagi. | password is incorrect |
      | dfghjkas  | userPassword | Email/nomor HP-nya tidak valid.     | email is incorrect    |

  @Negative
  Scenario Outline: cannot login blibli mobile without filled in username and/or password
    Given [Subscription-UI-Mobile] user open blibli app
    When [Subscription-UI-Mobile] user click masuk
    And [Subscription-UI-Mobile] user type '<username>' and '<password>'
    And [Subscription-UI-Mobile] user click login button form
    Then [Subscription-UI-Mobile] user should see the login button is disabled
    Examples:
      | username  | password     |
      |           |              |
      |           | userPassword |
      | userEmail |              |