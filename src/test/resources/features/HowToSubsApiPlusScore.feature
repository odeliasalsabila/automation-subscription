@HowToSubsAPIPlusScoreFeature
Feature: How To Subscribe Feature

  Scenario: Get text how to subscribe from API
    Given [Subscription-API-Desktop] user send get blm subs section request
    Then [Subscription-API-Desktop] user should see get blm subs section response with status code 200
    And [Subscription-API-Desktop] user should see get blm subs section response with status message 'OK'
    When [Subscription-API-Desktop] user save the title from blm subs section response
    And [Subscription-API-Desktop] user save the text as value with key to file csv 'resultAPI'
