@LanggananButtonPDPFeature @Subscription @Desktop @UI
  Feature: Subscription Button On PDP

    @Positive
    Scenario: Verify the subscription button is clickable when product subs is available
      Given [Subscription-UI-Desktop] user at Bliblicom homepage
      When [Subscription-UI-Desktop] user click nanti saja on access location
      And [Subscription-UI-Desktop] user click semua favorit kamu
      And [Subscription-UI-Desktop] user close the iframe homepage
      And [Subscription-UI-Desktop] user choose bliblimart category
      And [Subscription-UI-Desktop] user click langganan tab
      And [Subscription-UI-Desktop] user click lihat semua on microsite page
      And [Subscription-UI-Desktop] user click mantap on top rated onborading
      And [Subscription-UI-Desktop] user click hide the oos product
      And [Subscription-UI-Desktop] user click first product on product list page
      Then [Subscription-UI-Desktop] user should see onboarding subscription if exist
      And [Subscription-UI-Desktop] user click oke on subscription onboarding if exist
      Then [Subscription-UI-Desktop] user should redirect to the PDP according to the product selected
      And [Subscription-UI-Desktop] user should see that the subscription button is clickable