@SubscriptionFeature @Subscription @Desktop @UI
Feature: Subscription feature

  Background:
    Given [Subscription-UI-Desktop] user at Bliblicom homepage
    When [Subscription-UI-Desktop] user click nanti saja on access location
    And [Subscription-UI-Desktop] user close the iframe homepage

  @Positive
  Scenario Outline: As a customer I want to subscribe new product
    And [Subscription-UI-Desktop] user click link login
    And [Subscription-UI-Desktop] user input email with value 'userEmail'
    And [Subscription-UI-Desktop] user input password with value 'userPassword'
    And [Subscription-UI-Desktop] user click button masuk
    And [Subscription-UI-Desktop] user click email validation button
    And [Google-Mail-API] user read the new inbox in mail using 'userEmail' and 'userPassword'
    And [Subscription-UI-Desktop] user fill the code of email validation
    And [Subscription-UI-Desktop] user click verifikasi button of email verification form
    And [Subscription-UI-Desktop] user click nanti saja on verification page
    Then [Subscription-UI-Desktop] user should be at the homepage
    When [Subscription-UI-Desktop] user click semua favorit kamu
    And [Subscription-UI-Desktop] user choose bliblimart category
    And [Subscription-UI-Desktop] user click langganan tab
    And [Subscription-UI-Desktop] user click lihat semua on microsite page
    And [Subscription-UI-Desktop] user click mantap on top rated onborading
    And [Subscription-UI-Desktop] user click hide the oos product
    And [Subscription-UI-Desktop] user click first product on product list page
    Then [Subscription-UI-Desktop] user should see onboarding subscription if exist
    And [Subscription-UI-Desktop] user click oke on subscription onboarding if exist
    Then [Subscription-UI-Desktop] user should redirect to the PDP according to the product selected
    And [Subscription-UI-Desktop] user should see that the subscription button is clickable
    When [Subscription-UI-Desktop] user click subscription button
    And [Subscription-UI-Desktop] user click oke on subscription onboarding if exist
    And [Subscription-UI-Desktop] user click siap on subscription onboarding if exist
    And [Subscription-UI-Desktop] user choose paket langganan with '<values>'
    And [Subscription-UI-Desktop] user fill quantity with '<quantity>'
    And [Subscription-UI-Desktop] user click tambah ke langganan button
    Then [Subscription-UI-Desktop] user should see the subscription success's popup
    And [Subscription-UI-Desktop] user should see lihat langganan button is clickable
    When [Subscription-UI-Desktop] user click lihat langganan button
    Then [Subscription-UI-Desktop] user should see the correct product subscription according to '<bundles>'
    When [Subscription-UI-Desktop] user click delete icon on product subscription
    And [Subscription-UI-Desktop] user click delete button for clarification
    Then [Subscription-UI-Desktop] user should not see those product subscription again according to '<bundles>'
    Examples:
      | values   | quantity | bundles  |
      | 5 Minggu | 2        | Mingguan |

  @Positive
  Scenario Outline: As a customer I want to subscribe new product without logging in first
    And [Subscription-UI-Desktop] user click semua favorit kamu
    And [Subscription-UI-Desktop] user choose bliblimart category
    And [Subscription-UI-Desktop] user click langganan tab
    And [Subscription-UI-Desktop] user click lihat semua on microsite page
    And [Subscription-UI-Desktop] user click mantap on top rated onborading
    And [Subscription-UI-Desktop] user click hide the oos product
    And [Subscription-UI-Desktop] user click first product on product list page
    Then [Subscription-UI-Desktop] user should see onboarding subscription if exist
    And [Subscription-UI-Desktop] user click oke on subscription onboarding if exist
    Then [Subscription-UI-Desktop] user should redirect to the PDP according to the product selected
    And [Subscription-UI-Desktop] user should see that the subscription button is clickable
    When [Subscription-UI-Desktop] user click subscription button
    Then [Subscription-UI-Desktop] user should see login form
    When [Subscription-UI-Desktop] user input email with value 'userEmail'
    And [Subscription-UI-Desktop] user input password with value 'userPassword'
    And [Subscription-UI-Desktop] user click button masuk
    And [Subscription-UI-Desktop] user click email validation button
    And [Google-Mail-API] user read the new inbox in mail using 'userEmail' and 'userPassword'
    And [Subscription-UI-Desktop] user fill the code of email validation
    And [Subscription-UI-Desktop] user click verifikasi button of email verification form
    And [Subscription-UI-Desktop] user click nanti saja on verification page
    And [Subscription-UI-Desktop] user click oke on subscription onboarding if exist
    And [Subscription-UI-Desktop] user click subscription button
    And [Subscription-UI-Desktop] user click siap on subscription onboarding if exist
    And [Subscription-UI-Desktop] user choose paket langganan with '<values>'
    And [Subscription-UI-Desktop] user fill quantity with '<quantity>'
    And [Subscription-UI-Desktop] user click tambah ke langganan button
    Then [Subscription-UI-Desktop] user should see the subscription success's popup
    And [Subscription-UI-Desktop] user should see lihat langganan button is clickable
    When [Subscription-UI-Desktop] user click lihat langganan button
    Then [Subscription-UI-Desktop] user should see the correct product subscription according to '<bundles>'
    When [Subscription-UI-Desktop] user click delete icon on product subscription
    And [Subscription-UI-Desktop] user click delete button for clarification
    Then [Subscription-UI-Desktop] user should not see those product subscription again according to '<bundles>'
    Examples:
      | values  | quantity | bundles |
      | 2 Bulan | 2        | Bulanan |

  @Positive
  Scenario Outline: Subscribe product that already subscribed
    And [Subscription-UI-Desktop] user click link login
    And [Subscription-UI-Desktop] user input email with value 'userEmail'
    And [Subscription-UI-Desktop] user input password with value 'userPassword'
    And [Subscription-UI-Desktop] user click button masuk
    And [Subscription-UI-Desktop] user click email validation button
    And [Google-Mail-API] user read the new inbox in mail using 'userEmail' and 'userPassword'
    And [Subscription-UI-Desktop] user fill the code of email validation
    And [Subscription-UI-Desktop] user click verifikasi button of email verification form
    And [Subscription-UI-Desktop] user click nanti saja on verification page
    Then [Subscription-UI-Desktop] user should be at the homepage
    When [Subscription-UI-Desktop] user search product subscription using '<product sku>'
    And [Subscription-UI-Desktop] user click icon search
    And [Subscription-UI-Desktop] user click siap on two our delivery onborading
    And [Subscription-UI-Desktop] user click first product on product list page
    Then [Subscription-UI-Desktop] user should see onboarding subscription if exist
    When [Subscription-UI-Desktop] user click oke on subscription onboarding if exist
    Then [Subscription-UI-Desktop] user should redirect to the PDP according to the product selected
    And [Subscription-UI-Desktop] user should see that the subscription button is clickable
    When [Subscription-UI-Desktop] user click subscription button
    And [Subscription-UI-Desktop] user click oke on subscription onboarding if exist
    And [Subscription-UI-Desktop] user click siap on subscription onboarding if exist
    When [Subscription-UI-Desktop] user click buat langganan baru
    And [Subscription-UI-Desktop] user choose paket langganan with '<values>'
    And [Subscription-UI-Desktop] user fill quantity with '<quantity>'
    And [Subscription-UI-Desktop] user click tambah ke langganan button
    Then [Subscription-UI-Desktop] user should see the subscription success's popup
    And [Subscription-UI-Desktop] user should see lihat langganan button is clickable
    When [Subscription-UI-Desktop] user click lihat langganan button
    Then [Subscription-UI-Desktop] user should see the correct product subscription according to '<bundles>'
    Examples:
      | values   | quantity | product sku   | bundles  |
      | 3 Bulan  | 2        | subscription2 | Bulanan  |
      | 3 Minggu | 1        | subscription2 | Mingguan |



