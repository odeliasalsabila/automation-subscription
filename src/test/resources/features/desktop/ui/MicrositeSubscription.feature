@MicrositeSubscriptionFeature @Subscription @Desktop @UI
  Feature: Microsite Subscription Feature

    Background:
      Given [Subscription-UI-Desktop] user at Bliblicom homepage
      When [Subscription-UI-Desktop] user click nanti saja on access location
      And [Subscription-UI-Desktop] user close the iframe homepage
      And [Subscription-UI-Desktop] user click semua favorit kamu
      And [Subscription-UI-Desktop] user choose bliblimart category
      And [Subscription-UI-Desktop] user click langganan tab

    @Positive @CheckLanggananPage
    Scenario: As a customer i want to access langganan page
      Then [Subscription-UI-Desktop] user should see subscription banner
      And [Subscription-UI-Desktop] user should see cara berlangganan blimart section
      And [Subscription-UI-Desktop] user should see blimart product set

    @Positive @TermAndCondition
    Scenario: Succesfully open the popup of subscription term and condition
      Then [Subscription-UI-Desktop] user should see cara berlangganan blimart section
      When [Subscription-UI-Desktop] user click term and condition link
      Then [Subscription-UI-Desktop] user should see the popup term and condition
      When [Subscription-UI-Desktop] user click close the popup term and condition
      Then [Subscription-UI-Desktop] user should not see the popup term and condition