@LoginDesktopFeature @Subscription @Desktop @UI
Feature: Login into Blibli desktop feature

  Background:
    Given [Subscription-UI-Desktop] user at Bliblicom homepage
    When [Subscription-UI-Desktop] user click nanti saja on access location
    And [Subscription-UI-Desktop] user close the iframe homepage
    And [Subscription-UI-Desktop] user click link login

  @Positive
  Scenario: Successfully logged into Blibli using registered account
    And [Subscription-UI-Desktop] user input email with value 'userEmail'
    And [Subscription-UI-Desktop] user input password with value 'userPassword'
    And [Subscription-UI-Desktop] user click button masuk
    And [Subscription-UI-Desktop] user click email validation button
    And [Google-Mail-API] user read the new inbox in mail using 'userEmail' and 'userPassword'
    And [Subscription-UI-Desktop] user fill the code of email validation
    And [Subscription-UI-Desktop] user click verifikasi button of email verification form
    And [Subscription-UI-Desktop] user click nanti saja on verification page
    Then [Subscription-UI-Desktop] user should be at the homepage
    When [Subscription-UI-Desktop] user click account
    Then [Subscription-UI-Desktop] user should see email as previously entered on info data account

  @Negative
  Scenario Outline: failed to login into Blibli Desktop with scenario '<negative scenario>'
    And [Subscription-UI-Desktop] user input email with value '<emailTxt>'
    And [Subscription-UI-Desktop] user input password with value '<passwordTxt>'
    And [Subscription-UI-Desktop] user click button masuk
    Then [Subscription-UI-Desktop] user should see error's ticker
    And [Subscription-UI-Desktop] user should see '<errorMessage>' for '<negative scenario>'
    Examples:
      | emailTxt  | passwordTxt  | errorMessage                        | negative scenario     |
      | userEmail | sdfghjk      | Yakin itu benar? Coba diingat lagi. | password is incorrect |
      | dfghjkas  | userPassword | Email/nomor HP-nya tidak valid      | email is incorrect    |

  @Negative
  Scenario Outline: cannot login without filled in username and/or password
    And [Subscription-UI-Desktop] user input email with value '<emailTxt>'
    And [Subscription-UI-Desktop] user input password with value '<passwordTxt>'
    Then [Subscription-UI-Desktop] user should see the login button is disabled
    Examples:
      | emailTxt  | passwordTxt  |
      |           |              |
      |           | userPassword |
      | userEmail |              |