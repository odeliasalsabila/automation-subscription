@LoginDesktopAPIFeature @Subscription @Desktop @API
Feature: Login Blibli Desktop using API

  Background:
    Given [Subscription-UI-Desktop] user at Bliblicom homepage
    When [Subscription-UI-Desktop] user click nanti saja on access location
    And [Subscription-UI-Desktop] user close the iframe homepage
    And [Subscription-UI-Desktop] user click link login

  @Positive
  Scenario: Verify user has been logged in using API
    And [Subscription-UI-Desktop] user input email with value 'userEmail'
    And [Subscription-UI-Desktop] user input password with value 'userPassword'
    And [Subscription-UI-Desktop] user click button masuk
    And [Subscription-UI-Desktop] user click email validation button
    And [Subscription-UI-Desktop] the session not expired
    And [Google-Mail-API] user read the new inbox in mail using 'userEmail' and 'userPassword'
    And [Subscription-UI-Desktop] user fill the code of email validation
    And [Subscription-UI-Desktop] the session not expired
    And [Subscription-UI-Desktop] user click verifikasi button of email verification form
    Then [Subscription-UI-Desktop] user should be at the homepage
    And [Subscription-UI-Desktop] user close the iframe homepage
    When [Subscription-UI-Desktop] user click account
    Then [Subscription-UI-Desktop] user should see email as previously entered on info data account
    Given [Subscription-API-Desktop] user get cookies
    And [Subscription-API-Desktop] user prepare data get user has logged in with cookies
    When [Subscription-API-Desktop] user send get user has logged in request
    Then [Subscription-API-Desktop] user should see get user has logged in response with status code 200
    And [Subscription-API-Desktop] user should see get user has logged in response with status message 'OK'
    And [Subscription-API-Desktop] user should see get user has logged in response with email 'userEmail'

  @Negative
  Scenario Outline: Failed to login into Blibli account using API
    And [Subscription-UI-Desktop] user input email with value '<email>'
    And [Subscription-UI-Desktop] user input password with value '<password>'
    And [Subscription-UI-Desktop] user click button masuk
    Given [Subscription-API-Desktop] user get cookies
    And [Subscription-API-Desktop] user prepare data get user has logged in with cookies
    When [Subscription-API-Desktop] user send get user has logged in request
    Then [Subscription-API-Desktop] user should see get user has logged in response with status code 401
    And [Subscription-API-Desktop] user should see get user has logged in response with status message 'UNAUTHORIZED'
    Examples:
      | email            | password |
      | future@gmail.com | 1234     |

