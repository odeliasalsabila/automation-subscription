@SubscriptionProductsDesktopAPIFeature @Subscription @Desktop @API
  Feature: Subscription Products API Feature

    @Positive
    Scenario: Verify get subscription products API successfully
      Given [Subscription-UI-Desktop] user at Bliblicom homepage
      When [Subscription-UI-Desktop] user click nanti saja on access location
      And [Subscription-UI-Desktop] user click link login
      And [Subscription-UI-Desktop] user input email with value 'userEmail'
      And [Subscription-UI-Desktop] user close the iframe homepage
      And [Subscription-UI-Desktop] user input password with value 'userPassword'
      And [Subscription-UI-Desktop] user click button masuk
      And [Subscription-UI-Desktop] user click email validation button
      And [Google-Mail-API] user read the new inbox in mail using 'userEmail' and 'userPassword'
      And [Subscription-UI-Desktop] user fill the code of email validation
      And [Subscription-UI-Desktop] user click verifikasi button of email verification form
      And [Subscription-UI-Desktop] user click nanti saja on verification page
      Then [Subscription-UI-Desktop] user should be at the homepage
      And [Subscription-UI-Desktop] user search product subscription using 'subscription3'
      And [Subscription-UI-Desktop] user close the iframe homepage
      And [Subscription-UI-Desktop] user click icon search
      And [Subscription-UI-Desktop] user click siap on two our delivery onborading
      And [Subscription-UI-Desktop] user click first product on product list page
      Then [Subscription-UI-Desktop] user should see onboarding subscription if exist
      When [Subscription-UI-Desktop] user click oke on subscription onboarding if exist
      Then [Subscription-UI-Desktop] user should redirect to the PDP according to the product selected
      And [Subscription-UI-Desktop] user should see that the subscription button is clickable
      When [Subscription-UI-Desktop] user click subscription button
      Given [Subscription-API-Desktop] user get cookies
      And [Subscription-API-Desktop] user prepare data get user has logged in with cookies
      And [Subscription-API-Desktop] user prepare data get subscription products with item sku by 'subscription3'
      When [Subscription-API-Desktop] user send get subscription products request
      Then [Subscription-API-Desktop] user should see get subscription product response with status code 200
      And [Subscription-API-Desktop] user should see get subscription product response with status message 'OK'
      And [Subscription-API-Desktop] user should see get subscription product respone with item sku as the previous product added
      And [Subscription-API-Desktop] user should see get subscription product respone with name as the previous product added