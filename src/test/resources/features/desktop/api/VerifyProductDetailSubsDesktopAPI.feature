@VerifyProductDetailSubsDesktopAPIFeature @Subscription @Desktop @API
Feature: Product Detail API Feature

  @Positive
  Scenario: Verify product detail API for subscription product contains tag subscription
    Given [Subscription-UI-Desktop] user at Bliblicom homepage
    When [Subscription-UI-Desktop] user click nanti saja on access location
    And [Subscription-UI-Desktop] user close the iframe homepage
    And [Subscription-UI-Desktop] user search product subscription using 'subscription2'
    And [Subscription-UI-Desktop] user click icon search
    And [Subscription-UI-Desktop] user click siap on two our delivery onborading
    And [Subscription-UI-Desktop] user click first product on product list page
    Then [Subscription-UI-Desktop] user should see onboarding subscription if exist
    When [Subscription-UI-Desktop] user click oke on subscription onboarding if exist
    Then [Subscription-UI-Desktop] user should redirect to the PDP according to the product selected
    And [Subscription-UI-Desktop] user should see that the subscription button is clickable
    Given [Subscription-API-Desktop] user prepare data get product detail with 'subscription2'
    And [Subscription-API-Desktop] user prepare data get product detail with product code by 'productCode'
    When [Subscription-API-Desktop] user send get product detail request
    Then [Subscription-API-Desktop] user should see product detail response with status code 200
    And [Subscription-API-Desktop] user should see product detail response with status message 'OK'
    And [Subscription-API-Desktop] user should see product detail response with tags contain 'SUBSCRIPTION'