@SubscribeDesktopAPIFeature @Subscription @Desktop @API
Feature: Subscription API Feature

  @Positive
  Scenario Outline: Verify Subscription cart API post
    Given [Subscription-UI-Desktop] user at Bliblicom homepage
    When [Subscription-UI-Desktop] user click nanti saja on access location
    And [Subscription-UI-Desktop] user click link login
    And [Subscription-UI-Desktop] user input email with value 'userEmail'
    And [Subscription-UI-Desktop] user input password with value 'userPassword'
    And [Subscription-UI-Desktop] user click button masuk
    And [Subscription-UI-Desktop] user click email validation button
    And [Google-Mail-API] user read the new inbox in mail using 'userEmail' and 'userPassword'
    And [Subscription-UI-Desktop] user fill the code of email validation
    And [Subscription-UI-Desktop] user click verifikasi button of email verification form
    And [Subscription-UI-Desktop] user click nanti saja on verification page
    Then [Subscription-UI-Desktop] user should be at the homepage
    And [Subscription-UI-Desktop] user close the iframe homepage
    When [Subscription-UI-Desktop] user search product subscription using '<product sku>'
    And [Subscription-UI-Desktop] user click icon search
    And [Subscription-UI-Desktop] user click siap on two our delivery onborading
    And [Subscription-UI-Desktop] user click first product on product list page
    Then [Subscription-UI-Desktop] user should see onboarding subscription if exist
    When [Subscription-UI-Desktop] user click oke on subscription onboarding if exist
    Then [Subscription-UI-Desktop] user should redirect to the PDP according to the product selected
    And [Subscription-UI-Desktop] user should see that the subscription button is clickable
    When [Subscription-UI-Desktop] user click subscription button
    And [Subscription-UI-Desktop] user click oke on subscription onboarding if exist
    And [Subscription-UI-Desktop] user click siap on subscription onboarding if exist
    Given [Subscription-API-Desktop] user get cookies
    And [Subscription-API-Desktop] user prepare data get user has logged in with cookies
    And [Subscription-API-Desktop] user prepare data subscription cart with item sku by '<item sku>'
    And [Subscription-API-Desktop] user prepare data subscription cart with price from ui
    And [Subscription-API-Desktop] user prepare data subscription cart with quantity from ui
    And [Subscription-API-Desktop] user prepare data subscription cart with frequency from ui
    And [Subscription-API-Desktop] user prepare data subscription cart with period from ui
    When [Subscription-API-Desktop] user send subscription cart request
    Then [Subscription-API-Desktop] user should see subscription cart response with status code 200
    And [Subscription-API-Desktop] user should see subscription cart response with status message 'OK'
    And [Subscription-API-Desktop] user should see subscription cart response with item sku as the previous product added
    And [Subscription-API-Desktop] user should see subscription cart response with name as the previous product added
    When [Subscription-UI-Desktop] user go to subscription cart page
    Then [Subscription-UI-Desktop] user should see the correct product subscription according to '<bundles>'
    When [Subscription-UI-Desktop] user click delete icon on product subscription
    And [Subscription-UI-Desktop] user click delete button for clarification
    Then [Subscription-UI-Desktop] user should not see those product subscription again according to '<bundles>'
    Examples:
      | product sku   | item sku      | bundles |
      | subscription1 | subscription1 | Bulanan |
