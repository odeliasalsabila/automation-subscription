@HowToSubsUIPlusScoreFeature
Feature: How To Subscribe Feature

  Scenario: Get text how to subscribe from UI
    Given [Subscription-UI-Desktop] user at Bliblicom homepage
    When [Subscription-UI-Desktop] user click nanti saja on access location
    And [Subscription-UI-Desktop] user close the iframe homepage
    And [Subscription-UI-Desktop] user click semua favorit kamu
    And [Subscription-UI-Desktop] user choose bliblimart category
    And [Subscription-UI-Desktop] user click langganan tab
    Then [Subscription-UI-Desktop] user should see cara berlangganan blimart section
    When [Subscription-UI-Desktop] user get text for how to subscribe
    And [Subscription-UI-Desktop] user save the text as value with key to file csv 'resultUI'