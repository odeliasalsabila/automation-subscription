package com.gdn.qa.future.data.comparisonData;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component("com.gdn.qa.future.data.comparisonData.APIData")
public class APIData {
    private List<String> titles;
    private List<String> types;
}
