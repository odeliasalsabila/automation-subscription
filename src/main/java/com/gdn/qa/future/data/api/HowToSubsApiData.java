package com.gdn.qa.future.data.api;

import com.gdn.qa.future.api.response.getSubsSection.GetSubsSectionResponse;
import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component("com.gdn.qa.future.data.api.HowToSubsApiData")
public class HowToSubsApiData {
    private GetSubsSectionResponse getSubsSectionResponse;
}
