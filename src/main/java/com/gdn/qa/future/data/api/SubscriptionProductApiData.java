package com.gdn.qa.future.data.api;

import com.gdn.qa.future.api.response.getSubscriptionProduct.GetSubscriptionProductResponse;
import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component("com.gdn.qa.future.data.api.SubscriptionProductApiData")
public class SubscriptionProductApiData {
    private String itemSku;
    private String name;

    private GetSubscriptionProductResponse subscriptionProductResponse;
}
