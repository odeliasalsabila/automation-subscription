package com.gdn.qa.future.data.api;

import com.gdn.qa.future.api.response.getLoginUser.GetUserLoginResponse;
import io.restassured.http.Cookies;
import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component("com.gdn.qa.future.data.api.UserLoginApiData")
public class UserLoginApiData {
    private Cookies cookies;

    private GetUserLoginResponse loginResponse;
}
