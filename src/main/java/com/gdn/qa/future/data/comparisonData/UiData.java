package com.gdn.qa.future.data.comparisonData;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component("com.gdn.qa.future.data.comparisonData.UiData")
public class UiData {
    private List<String> titles;
}
