package com.gdn.qa.future.data.ui;

import com.gdn.qa.future.api.response.getLoginUser.GetUserLoginResponse;

import io.restassured.http.Cookies;
import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component("com.gdn.qa.future.data.ui.UserData")
public class UserData {
    private String emailUser;
    private String passwordUser;
    private String verificationCode;
    private Cookies loginCookies;
}
