package com.gdn.qa.future.data.api;

import com.gdn.qa.future.api.response.getSubscriptionCart.AddSubscriptionCartResponse;
import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component("com.gdn.qa.future.data.api.SubscriptionCartApiData")
public class SubscriptionCartApiData {
    private String itemSku;
    private int quantity;
    private int price;
    private String frequency;
    private int period;

    private AddSubscriptionCartResponse subscriptionCartResponse;
}
