package com.gdn.qa.future.data.ui;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component("com.gdn.qa.future.data.ui.ProductData")
public class ProductData {
    private String productName;
    private String productSku;
}
