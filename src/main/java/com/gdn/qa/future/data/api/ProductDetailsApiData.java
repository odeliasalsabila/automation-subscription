package com.gdn.qa.future.data.api;

import com.gdn.qa.future.api.response.getProductDetails.GetProductDetailsResponse;
import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component("com.gdn.qa.future.data.api.ProductDetailsApiData")
public class ProductDetailsApiData {
    private String productSku;
    private String productCode;

    private GetProductDetailsResponse productDetailsResponse;
}
