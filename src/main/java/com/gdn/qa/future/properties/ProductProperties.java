package com.gdn.qa.future.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Data
@Component("com.gdn.qa.future.properties.ProductProperties")
@ConfigurationProperties(prefix = "product")
public class ProductProperties {
    private HashMap<String, String> productSku;
    private HashMap<String, String> itemSku;
    private String productCode;
}
