package com.gdn.qa.future.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Data
@Component("com.gdn.qa.future.properties.ServiceProperties")
@ConfigurationProperties(prefix = "service")
public class ServiceProperties {
    private HashMap<String, String> headers;
    private HashMap<String, String> baseUrl;
}
