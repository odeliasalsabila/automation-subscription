package com.gdn.qa.future.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Data
@Component("com.gdn.qa.future.properties.DefaultProperties")
@ConfigurationProperties(prefix = "default")
public class DefaultProperties {
    private HashMap<String, String> cookies;
}
