package com.gdn.qa.future.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component("com.gdn.qa.future.properties.UserProperties")
@ConfigurationProperties(prefix = "user")
public class UserProperties {
    private String email;
    private String password;
    private String passwordGmail;
}
