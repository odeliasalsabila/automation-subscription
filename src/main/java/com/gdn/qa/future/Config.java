package com.gdn.qa.future;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;

@EnableConfigurationProperties
@ComponentScan(
        basePackages={"com.gdn.qa.future"}
)
public class Config {
}
