package com.gdn.qa.future.pages.mobile;

import com.gdn.qa.future.locators.mobile.ProductListAppUI;
import com.gdn.qa.future.utils.MobileUtil;
import org.springframework.stereotype.Component;

import java.time.Duration;

@Component("com.gdn.qa.future.pages.mobile.ProductListPageMobile")
public class ProductListPageMobile extends MobileUtil {

    public void clickMantap() {
        find(ProductListAppUI.MANTAP_TOPRATED_ONBORADING).withTimeoutOf(Duration.ofSeconds(10)).waitUntilVisible();
        if (find(ProductListAppUI.MANTAP_TOPRATED_ONBORADING).isVisible()){
            find(ProductListAppUI.MANTAP_TOPRATED_ONBORADING).click();
        }
    }

    public void clickFilter() {
        clickByCoordinatesUsingId(find(ProductListAppUI.TAMBAHKAN_KE_BAG_BUTTON), 184, 198);
    }

    public void clickHideOosProduct() {
        scrollToElement(find(ProductListAppUI.HIDE_OOS_PRODUCT_TEXT), 4);
        find(ProductListAppUI.HIDE_OOS_PRODUCT).click();
    }

    public void clickFirstProductBlimart() {
        find(ProductListAppUI.PRODUCT_BLIMART).click();
    }

    public void clickTerapkan() {
        find(ProductListAppUI.TERAPKAN_BUTTON).click();
    }

    public String getProductName() {
        find(ProductListAppUI.PRODUCT_BLIMART).withTimeoutOf(Duration.ofSeconds(60));
        return find(ProductListAppUI.PRODUCT_BLIMART).getText();
    }
}
