package com.gdn.qa.future.pages.mobile;

import com.gdn.qa.future.locators.mobile.LamaBerlanggananAppUI;
import com.gdn.qa.future.utils.MobileUtil;
import org.springframework.stereotype.Component;

@Component("com.gdn.qa.future.pages.mobile.LamaBerlanggananMobilePage")
public class LamaBerlanggananMobilePage extends MobileUtil {

    public void fillLamaBerlangganan(String value) {
        if (value.contains("bulan")) {
            find(LamaBerlanggananAppUI.getLamaBerlanggananFrequency("Bulanan")).click();
        } else if (value.contains("minggu")) {
            find(LamaBerlanggananAppUI.getLamaBerlanggananFrequency("Mingguan")).click();
        }
        find(LamaBerlanggananAppUI.PERIOD_BUTTON).click();
        scrollToElement(find(LamaBerlanggananAppUI.getListPeriodSubscription(value)), 3);
        find(LamaBerlanggananAppUI.getListPeriodSubscription(value)).click();
    }

    public void fillQuantity(String qty) {
        find(LamaBerlanggananAppUI.QUANTITY).type(qty);
    }

    public void clickTambahKeLanggananButton() {
        find(LamaBerlanggananAppUI.TAMBAH_KE_BAG).click();
    }

    public boolean getSuccessSubsribePopup() {
        return find(LamaBerlanggananAppUI.SUBSCRIBE_SUCCESS_POPUP).isDisplayed();
    }

    public boolean getLihatLanggananButton() {
        return find(LamaBerlanggananAppUI.LIHAT_LANGGANAN_BUTTON).isClickable();
    }

    public void clickLihatLanggananButton() {
        find(LamaBerlanggananAppUI.LIHAT_LANGGANAN_BUTTON).click();
    }
}
