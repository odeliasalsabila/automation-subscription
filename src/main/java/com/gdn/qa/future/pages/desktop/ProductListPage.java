package com.gdn.qa.future.pages.desktop;

import com.gdn.qa.future.locators.desktop.ProductListUI;
import com.gdn.qa.future.utils.WebElementHelper;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.springframework.stereotype.Component;

@Component("com.gdn.qa.future.pages.desktop.ProductListPage")
public class ProductListPage extends WebElementHelper {
    public void clickHideOosFilter() {
        clickElementJavascript(find(ProductListUI.HIDE_OOS_LABEL));
        clickElementJavascript(find(ProductListUI.HIDE_OOS));
    }

    public String getProductName() {
        waitABit(2000);
        waitFor(ExpectedConditions.visibilityOfElementLocated(ProductListUI.PRODUCT_NAME));
        return find(ProductListUI.PRODUCT_NAME).getText();
    }

    public void clickProductSubscription() {
        clickElementJavascript(find(ProductListUI.PRODUCT_NAME));
        String winHandleBefore = getWindowHandle();
        waitForNewWindow(getDriver(), 10);
        windowsHandleCurrent(winHandleBefore);
    }
}
