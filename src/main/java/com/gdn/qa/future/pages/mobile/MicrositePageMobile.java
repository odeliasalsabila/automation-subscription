package com.gdn.qa.future.pages.mobile;

import com.gdn.qa.future.locators.desktop.LoginDesktopUI;
import com.gdn.qa.future.locators.mobile.MicrositeAppUI;
import com.gdn.qa.future.utils.MobileUtil;
import lombok.SneakyThrows;
import net.serenitybdd.core.pages.WebElementFacade;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

@Component("com.gdn.qa.future.pages.mobile.MicrositePageMobile")
public class MicrositePageMobile extends MobileUtil {
    public void clickLanggananTab() {
        find(MicrositeAppUI.LANGGANAN_TAB).withTimeoutOf(Duration.ofSeconds(60));
        List<WebElementFacade> itemsTab = findAll(MicrositeAppUI.LANGGANAN_TAB);
        WebElementFacade langgananTab = itemsTab.get(5);
        langgananTab.withTimeoutOf(Duration.ofSeconds(60));
        langgananTab.click();
    }

    public boolean getBannerSubs() {
        return find(MicrositeAppUI.BANNER_SUBS).isDisplayed();
    }

    public boolean getHowToSubscribe() {
        scrollToElement(find(MicrositeAppUI.HOW_TO_SUBS_SECTION), 3);
        return find(MicrositeAppUI.HOW_TO_SUBS_SECTION).isDisplayed();
    }

    public boolean getBlimartProductSet() {
        return find(MicrositeAppUI.PRODUCT_SET).isDisplayed();
    }

    public void clickTnCSubs() {
        scrollUpByCount(1);
        List<WebElementFacade> itemsView = findAll(MicrositeAppUI.TERM_N_COND);
        System.out.println(itemsView.size());
        WebElementFacade syaratKetentuan = itemsView.get(9);
        scrollToElement(syaratKetentuan, 3);
        syaratKetentuan.withTimeoutOf(Duration.ofSeconds(7));
        syaratKetentuan.click();
    }

    public void clickLihatSemuaOnProductSet() {
        find(MicrositeAppUI.LIHAT_SEMUA).click();
    }

    public List<String> getTitleHowToSubs() {
        scrollUpByCount(2);
        scrollDownToElement(find(MicrositeAppUI.HOW_TO_SUBS_TITLE_TEXT), 3);
        List<WebElementFacade> itemsTitleHowToSubs = findAll(MicrositeAppUI.HOW_TO_SUBS_TITLE_TEXT);
        List<String> allTitleHowToSubs = new ArrayList<>();
        for (WebElementFacade itemsTitleHowToSub : itemsTitleHowToSubs) {
            allTitleHowToSubs.add(itemsTitleHowToSub.getText());
        }
        return allTitleHowToSubs;
    }
}
