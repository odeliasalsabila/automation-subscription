package com.gdn.qa.future.pages.desktop;

import com.gdn.qa.future.locators.desktop.FavoritSectionUI;
import com.gdn.qa.future.utils.WebElementHelper;
import org.springframework.stereotype.Component;

@Component("com.gdn.qa.future.pages.desktop.FavoritesPage")
public class FavoritesPage extends WebElementHelper {
    public void clickBliblimartCategory() {
        clickElementJavascript(find(FavoritSectionUI.BLIBLIMART_CATEGORY));
    }
}
