package com.gdn.qa.future.pages.mobile;

import com.gdn.qa.future.locators.mobile.ProductListAppUI;
import com.gdn.qa.future.utils.MobileUtil;
import org.openqa.selenium.By;
import org.springframework.stereotype.Component;

@Component("com.gdn.qa.future.pages.mobile")
public class OnBoardingMobilePage extends MobileUtil {

    public static final By SIAP_BUTTON = By.xpath("//android.widget.TextView[@text='Siap']");

    public boolean getMantapButtonOnBoarding() {
        if (find(ProductListAppUI.MANTAP_TOPRATED_ONBORADING).isVisible()) {
            return find(ProductListAppUI.MANTAP_TOPRATED_ONBORADING).isDisplayed();
        }
        return false;
    }
    
}
