package com.gdn.qa.future.pages.desktop;

import com.gdn.qa.future.locators.desktop.CartDesktopUI;
import com.gdn.qa.future.utils.WebElementHelper;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("com.gdn.qa.future.pages.desktop.SubscriptionCartPage")
@DefaultUrl("https://www.blibli.com/subscription/cart")
public class SubscriptionCartPage extends WebElementHelper {
    public List<String> getListProductName(String bundles) {
        ArrayList<String> listProductName = new ArrayList<>();
        List<WebElementFacade> productNameList = findAll(CartDesktopUI.listProductSubs(bundles));
        for(WebElementFacade webElementFacade : productNameList) {
            listProductName.add(webElementFacade.getText());
        }
        return listProductName;
    }

    public void clickTrashIconOnProduct(String productName) {
        find(CartDesktopUI.trashIconOnProduct(productName)).click();
    }

    public void clickDeleteClarification() {
        find(CartDesktopUI.DELETE_CLARIFICATION_BUTTON).click();
    }
}
