package com.gdn.qa.future.pages.desktop;

import com.gdn.qa.future.locators.desktop.OnBoardingDesktopUI;
import com.gdn.qa.future.utils.WebElementHelper;
import org.springframework.stereotype.Component;

@Component("com.gdn.qa.future.pages.desktop.OnBoardingDesktopPage")
public class OnBoardingDesktopPage extends WebElementHelper {
    public void clickSiapOnTwoDelivery() {
        if (find(OnBoardingDesktopUI.SIAP_TWO_DELIVERY_BUTTON).isVisible()) {
            find(OnBoardingDesktopUI.SIAP_TWO_DELIVERY_BUTTON).click();
        }
    }

    public void clickMantapTopRatedOnborading() {
        if (find(OnBoardingDesktopUI.MANTAP_TOPRATED_ONBORADING).isVisible()) {
            find(OnBoardingDesktopUI.MANTAP_TOPRATED_ONBORADING).click();
        }
    }

    public boolean getOnboardingSubs() {
        return find(OnBoardingDesktopUI.SUBS_ONBOARDING).isVisible();
    }

    public void clickOkeOnSubsOnboarding() {
        find(OnBoardingDesktopUI.OKE_SUBS_ONBOARDING).click();
        waitForAngularRequestsToFinish();
    }

    public boolean getOkeOnSubsOnboarding() {
        if (find(OnBoardingDesktopUI.OKE_SUBS_ONBOARDING).isVisible()) {
            return find(OnBoardingDesktopUI.SUBS_ONBOARDING).isDisplayed();
        }
        return false;
    }

    public boolean getSiapOnBoardingSubs() {
        if (find(OnBoardingDesktopUI.SIAP_BUTTON).isVisible()) {
            return find(OnBoardingDesktopUI.SIAP_BUTTON).isDisplayed();
        }
        return false;
    }

    public void clickSiapOnOnboardingSubs() {
        find(OnBoardingDesktopUI.SIAP_BUTTON).click();
    }
}
