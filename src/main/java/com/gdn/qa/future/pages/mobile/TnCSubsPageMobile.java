package com.gdn.qa.future.pages.mobile;

import com.gdn.qa.future.locators.mobile.MicrositeAppUI;
import com.gdn.qa.future.locators.mobile.TermCondSubsAppUI;
import com.gdn.qa.future.utils.MobileUtil;
import net.serenitybdd.core.pages.WebElementFacade;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.List;

@Component("com.gdn.qa.future.pages.mobile.TnCSubsPageMobile")
public class TnCSubsPageMobile extends MobileUtil {
    public boolean getTermnCondPopup() {
        return find(TermCondSubsAppUI.TERM_N_COND).isDisplayed();
    }

    public void clickClosePopup() {
        find(TermCondSubsAppUI.CLOSE_ICON).click();
    }

    public void getTermnCondLink() {
        scrollUpByCount(2);
        scrollDownToElement(find(MicrositeAppUI.HOW_TO_SUBS_SECTION),3);
        List<WebElementFacade> itemsView = findAll(MicrositeAppUI.TERM_N_COND);
        WebElementFacade syaratKetentuan = itemsView.get(9);
        scrollDownToElement(syaratKetentuan, 3);
        System.out.println(itemsView.size());
    }
}
