package com.gdn.qa.future.pages.mobile;

import com.gdn.qa.future.locators.mobile.ProductDetailAppUI;
import com.gdn.qa.future.utils.MobileUtil;
import org.springframework.stereotype.Component;

@Component("com.gdn.qa.future.pages.mobile.ProductDetailPageMobile")
public class ProductDetailPageMobile extends MobileUtil {
    public boolean getOnboardingSubs() {
        if (find(ProductDetailAppUI.ONBOARDING_SUBS).isDisplayed()) {
            return find(ProductDetailAppUI.ONBOARDING_SUBS).isDisplayed();
        }
        return false;
    }

    public boolean getOkeOnOnboardingSubs() {
        if (find(ProductDetailAppUI.OKE_BUTTON_ONBOARDING_SUBS).isDisplayed()) {
            return find(ProductDetailAppUI.OKE_BUTTON_ONBOARDING_SUBS).isDisplayed();
        }
        return false;
    }

    public void clickOkeOnOnboardingSubs() {
        if (find(ProductDetailAppUI.OKE_BUTTON_ONBOARDING_SUBS).isDisplayed()) {
            find(ProductDetailAppUI.OKE_BUTTON_ONBOARDING_SUBS).click();
        }
    }

    public boolean getBerlanggananButton() {
        return find(ProductDetailAppUI.BERLANGGANAN_BUTTON).isClickable();
    }

    public void clickBerlanggananButton() {
        find(ProductDetailAppUI.BERLANGGANAN_BUTTON).click();
    }

    public void clickSiapOnOnboardingSubs() {
        find(OnBoardingMobilePage.SIAP_BUTTON).click();
    }
}
