package com.gdn.qa.future.pages.desktop;

import com.gdn.qa.future.locators.desktop.ProductDetailUI;
import com.gdn.qa.future.utils.WebElementHelper;
import org.springframework.stereotype.Component;

@Component("com.gdn.qa.future.pages.desktop.ProductDetailPage")
public class ProductDetailPage extends WebElementHelper {
    public String getProductName() {
        return find(ProductDetailUI.PRODUCT_NAME).getText();
    }

    public boolean getSubsButton() {
        return find(ProductDetailUI.SUBS_BUTTON).isClickable();
    }

    public void clickSubsButton() {
        clickElementJavascript(find(ProductDetailUI.SUBS_BUTTON));
    }
}
