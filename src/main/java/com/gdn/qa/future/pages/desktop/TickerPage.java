package com.gdn.qa.future.pages.desktop;

import com.gdn.qa.future.locators.desktop.TickerUI;
import com.gdn.qa.future.utils.WebElementHelper;
import org.springframework.stereotype.Component;

@Component("com.gdn.qa.future.pages.desktop.TickerPage")
public class TickerPage extends WebElementHelper {
    public boolean getTickerError() {
        return find(TickerUI.TICKER_ERROR).isDisplayed();
    }
    public String getErrorMessageLogin() {
        return find(TickerUI.TICKER_ERROR_MESSAGE).getText();
    }
}
