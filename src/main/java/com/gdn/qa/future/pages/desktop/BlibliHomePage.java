package com.gdn.qa.future.pages.desktop;

import com.gdn.qa.future.locators.desktop.CaraouselDesktopUI;
import com.gdn.qa.future.locators.desktop.FavoritSectionUI;
import com.gdn.qa.future.locators.desktop.PopupDesktopUI;
import com.gdn.qa.future.utils.WebElementHelper;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.springframework.stereotype.Component;

@Component("com.gdn.qa.future.pages.desktop.BlibliHomepage")
@DefaultUrl("https://www.blibli.com/")
public class BlibliHomePage extends WebElementHelper {

    public boolean viewCaraouselHomepage() {
        return find(CaraouselDesktopUI.CAROUSEL_XPATH).isDisplayed();
    }

    public void closePopUp() {
        find(PopupDesktopUI.CLOSE_BUTTON).click();
    }

    public void closeIframe() {
        if (find(PopupDesktopUI.IFRAME).isVisible()){
            WebElementFacade element = find(PopupDesktopUI.IFRAME);
            getDriver().switchTo().frame(element);
            find(PopupDesktopUI.CLOSE_BUTTON_IFRAME).click();
            getDriver().switchTo().defaultContent();
        }
    }

    public void clickSemuaFavoritnyaKamu() {
        clickElementJavascript(find(FavoritSectionUI.SEMUA_MENU));
        waitForAngularRequestsToFinish();
    }

    public void clickNantiSajaOnAccessLocation() {
        find(PopupDesktopUI.NANTI_SAJA_ACCESS_LOCATION).click();
    }
}
