package com.gdn.qa.future.pages.desktop;

import com.gdn.qa.future.locators.desktop.LanggananUI;
import com.gdn.qa.future.locators.desktop.TncPopupDesktopUI;
import com.gdn.qa.future.utils.WebElementHelper;
import org.springframework.stereotype.Component;

@Component("com.gdn.qa.future.pages.desktop.TncSubsDesktopPage")
public class TncSubsDesktopPage extends WebElementHelper {
    public void clickTermAndConditionLink() {
        clickElementJavascript(find(TncPopupDesktopUI.TNC_LINK));
    }

    public boolean viewTncPopup() {
        return find(TncPopupDesktopUI.TNC_POPUP).isDisplayed();
    }

    public void clickCloseButton() {
        clickElementJavascript(find(TncPopupDesktopUI.CLOSE_BUTTON));
    }

    public boolean tncPopupClosed() {
        return find(LanggananUI.HOW_TO_SUBS_SECTION).isDisplayed();
    }
}
