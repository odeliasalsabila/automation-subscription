package com.gdn.qa.future.pages.desktop;

import com.gdn.qa.future.locators.desktop.LanggananUI;
import com.gdn.qa.future.locators.desktop.LoginDesktopUI;
import com.gdn.qa.future.locators.desktop.PopupDesktopUI;
import com.gdn.qa.future.utils.WebElementHelper;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.springframework.stereotype.Component;

import java.time.Duration;

@Component("com.gdn.qa.future.pages.desktop.LoginDesktopFormPage")
public class LoginDesktopFormPage extends WebElementHelper {
    public void clickLoginButton() {
        find(LoginDesktopUI.LOGIN_BUTTON).click();
    }

    public void typeEmail(String emailTxt) {
        find(LoginDesktopUI.EMAIL_TEXTBOX).withTimeoutOf(Duration.ofSeconds(7));
        waitFor(ExpectedConditions.visibilityOf(find(LoginDesktopUI.EMAIL_TEXTBOX)));
        find(LoginDesktopUI.EMAIL_TEXTBOX).type(emailTxt);
    }

    public void typePassword(String passwordTxt) {
        find(LoginDesktopUI.PASSWORD_TEXTBOX).type(passwordTxt);
    }

    public void clickLoginButtonForm() {
        find(LoginDesktopUI.LOGIN_BUTTON_FORM).click();
        waitForAngularRequestsToFinish();
    }

    public boolean getFormLogin() {
        find(LoginDesktopUI.FORM_LOGIN).withTimeoutOf(Duration.ofSeconds(7));
        return find(LoginDesktopUI.FORM_LOGIN).isDisplayed();
    }

    public void clickEmailValidationBtn() {
        find(LoginDesktopUI.EMAIL_VALIDATION_BTN).click();
    }

    public void verificationCodeEmailForm(String verificationCode) {
        find(LoginDesktopUI.VERIFICATION_CODE_FORM).type(verificationCode);
    }

    public void clickVerifikasiButton() {
        find(LoginDesktopUI.VERIFIKASI_BTN).withTimeoutOf(Duration.ofSeconds(10));
        if (find(LoginDesktopUI.VERIFIKASI_BTN).isClickable()) {
            find(LoginDesktopUI.VERIFIKASI_BTN).click();
        }
    }

    public void clickNantiSaja() {
        clickElementJavascript(find(LoginDesktopUI.NANTI_SAJA_BUTTON));
    }

    public boolean getLoginButton() {
        return find(LoginDesktopUI.LOGIN_BUTTON_FORM).isDisabled();
    }

    public boolean verificationButtonEmailIsClickable() {
        return find(LoginDesktopUI.VERIFIKASI_BTN).isClickable();
    }

    public void closeVerifikasiEmailForm() {
        find(LoginDesktopUI.CLOSE_VERIFIKASI_EMAIL_BUTTON).click();
    }

    public void clickBatalkanConfirmation() {
        find(LoginDesktopUI.BATALKAN_BUTTON).click();
    }

    public boolean getVerificationCodeEmailForm() {
        return find(LoginDesktopUI.VERIFICATION_CODE_FORM).isClickable();
    }

    public boolean getSessionTextWarning() {
        if (find(LoginDesktopUI.WARNING_SESSION).isVisible()){
            return find(LoginDesktopUI.WARNING_SESSION).isDisplayed();
        }
        return false;
    }
}
