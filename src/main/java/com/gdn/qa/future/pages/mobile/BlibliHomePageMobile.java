package com.gdn.qa.future.pages.mobile;

import com.gdn.qa.future.locators.mobile.LoginAppUI;
import com.gdn.qa.future.utils.MobileUtil;
import org.springframework.stereotype.Component;

import java.time.Duration;

import static java.time.temporal.ChronoUnit.SECONDS;

@Component("com.gdn.qa.future.pages.mobile.BlibliHomePageMobile")
public class BlibliHomePageMobile extends MobileUtil {
    public void openBlibliApp() {
        find(LoginAppUI.LOGIN_BUTTON).withTimeoutOf(Duration.of(60, SECONDS))
                .waitUntilVisible();
    }

    public void clickLoginMenu() {
        find(LoginAppUI.LOGIN_BUTTON).click();
    }
}
