package com.gdn.qa.future.pages.desktop;

import com.gdn.qa.future.locators.desktop.SearchBarUI;
import com.gdn.qa.future.utils.WebElementHelper;
import org.springframework.stereotype.Component;

@Component("com.gdn.qa.future.pages.desktop.SearchDesktopPage")
public class SearchDesktopPage extends WebElementHelper {
    public void searchProduct(String productSku) {
        find(SearchBarUI.SEARCH_BAR).type(productSku);
    }

    public void clickSearchIcon() {
        find(SearchBarUI.SEARCH_ICON).click();
    }
}
