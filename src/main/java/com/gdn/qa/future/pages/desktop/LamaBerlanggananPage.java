package com.gdn.qa.future.pages.desktop;

import com.gdn.qa.future.locators.desktop.LamaBerlanggananUI;
import com.gdn.qa.future.utils.WebElementHelper;
import net.serenitybdd.core.pages.WebElementFacade;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("com.gdn.qa.future.pages.desktop.LamaBerlanggananPage")
public class LamaBerlanggananPage extends WebElementHelper {
    public void clickTambahKeLangganan() {
        find(LamaBerlanggananUI.TAMBAH_KE_LANGGANAN_BUTTON).click();
    }

    public void clickLihatLangganan() {
        find(LamaBerlanggananUI.LIHAT_LANGGANAN_BUTTON).click();
    }

    public void fillLamaBerlangganan(String values) {
        if (values.contains("Bulan")) {
            find(LamaBerlanggananUI.getLamaBerlanggananFrequency("Bulan")).click();
        } else if (values.contains("Minggu")) {
            find(LamaBerlanggananUI.getLamaBerlanggananFrequency("Minggu")).click();
        }
        find(LamaBerlanggananUI.PERIOD_BUTTON).click();
        List<WebElementFacade> listPeriodSubscription = findAll(LamaBerlanggananUI.LIST_PERIOD);
        for (WebElementFacade webElementFacade : listPeriodSubscription) {
            if (webElementFacade.getText().contains(values)) {
                scrollIntoView(webElementFacade);
                webElementFacade.click();
                break;
            }
        }
    }

    public void fillSubscriptionQUantity(int quantity) {
        if (Integer.parseInt(find(LamaBerlanggananUI.QUANTITY_SUBSCRIPTION).getAttribute("value")) < quantity) {
            find(LamaBerlanggananUI.INCREASE_QUANTITY_BUTTON).click();
        } else if (Integer.parseInt(find(LamaBerlanggananUI.QUANTITY_SUBSCRIPTION).getAttribute("value")) > quantity) {
            find(LamaBerlanggananUI.DECREASE_QUANTITY_BUTTON).click();
        }
    }

    public int getPrice() {
        String price = find(LamaBerlanggananUI.PRICE).getText();
        return Integer.parseInt(String.join("", price.split("[Rp.]+")));
    }

    public int getQuantity() {
        String quantity = find(LamaBerlanggananUI.QUANTITY_SUBSCRIPTION).getAttribute("value");
        return Integer.parseInt(quantity);
    }

    public String getFrequency() {
        String frequency = find(LamaBerlanggananUI.FREQUENCY_CHECKED_SUBSCRIPTION).getText();
        String convertFrequency;
        if (frequency.equalsIgnoreCase("bulanan")){
            convertFrequency = "MONTHLY";
        }else{
            convertFrequency = "WEEKLY";
        }
        return convertFrequency;
    }

    public int getPeriod() {
        String period = find(LamaBerlanggananUI.PERIOD_SUBSCRIPTION).getText();
        String[] splitNumber = period.split(" ");
        return Integer.parseInt(splitNumber[0]);
    }
}
