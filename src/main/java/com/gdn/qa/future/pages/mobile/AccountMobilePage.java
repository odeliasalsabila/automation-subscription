package com.gdn.qa.future.pages.mobile;

import com.gdn.qa.future.locators.mobile.AccountMobileUI;
import com.gdn.qa.future.utils.MobileUtil;
import org.springframework.stereotype.Component;

@Component("com.gdn.qa.future.pages.mobile.AccountMobilePage")
public class AccountMobilePage extends MobileUtil {
    public void clickAkun() {
        find(AccountMobileUI.AKUN_ICON).click();
    }

    public void clickUserInfo() {
        find(AccountMobileUI.USER_INFO).click();
    }

    public String getUserName() {
        return find(AccountMobileUI.USERNAME).getText();
    }
}
