package com.gdn.qa.future.pages.mobile;

import com.gdn.qa.future.locators.mobile.FavoritSectionAppUI;
import com.gdn.qa.future.utils.MobileUtil;
import org.openqa.selenium.By;
import org.springframework.stereotype.Component;

@Component("com.gdn.qa.future.pages.mobile.FavoritPageMobile")
public class FavoritPageMobile extends MobileUtil {
    public void clickSemua() {
        find(FavoritSectionAppUI.WIDGET_SEMUA).click();
    }

    public void clickBlimart() {
        scrollToElement(find(FavoritSectionAppUI.WIDGET_BLIBLIMART), 3);
        find(FavoritSectionAppUI.WIDGET_BLIBLIMART).click();
    }
}
