package com.gdn.qa.future.pages.desktop;

import com.gdn.qa.future.locators.desktop.LanggananUI;
import com.gdn.qa.future.locators.desktop.ProductListUI;
import com.gdn.qa.future.utils.WebElementHelper;
import net.serenitybdd.core.pages.WebElementFacade;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

@Component("com.gdn.qa.future.pages.desktop.LanggananPage")
public class LanggananPage extends WebElementHelper {
    public void clickLanggananTab() {
        find(LanggananUI.LANGGANAN_TAB).withTimeoutOf(Duration.ofSeconds(7));
        find(LanggananUI.LANGGANAN_TAB).click();
    }

    public boolean viewBanner() {
        find(LanggananUI.SUBSCRIPTION_BANNER).withTimeoutOf(Duration.ofSeconds(7));
        return find(LanggananUI.SUBSCRIPTION_BANNER).isDisplayed();
    }

    public boolean viewHowToSubsSection() {
        return find(LanggananUI.HOW_TO_SUBS_SECTION).isDisplayed();
    }

    public void clickLihatSemua() {
        find(LanggananUI.LIHAT_SEMUA).withTimeoutOf(Duration.ofSeconds(7));
        clickElementJavascript(find(LanggananUI.LIHAT_SEMUA));
    }

    public boolean viewProductSet() {
        return find(ProductListUI.PRODUCT_SET).isDisplayed();
    }

    public List<String> getHowToSubsText() {
        List<WebElementFacade> itemsTitleHowToSubs = findAll(LanggananUI.HOW_TO_SUBS_TITLE_TEXT);
        List<String> allTitleHowToSubs = new ArrayList<>();
        for (WebElementFacade itemsTitleHowToSub : itemsTitleHowToSubs) {
            allTitleHowToSubs.add(itemsTitleHowToSub.getText());
        }
        return allTitleHowToSubs;
    }
}
