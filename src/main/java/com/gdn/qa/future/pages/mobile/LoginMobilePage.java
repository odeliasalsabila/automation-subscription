package com.gdn.qa.future.pages.mobile;

import com.gdn.qa.future.locators.mobile.LoginAppUI;
import com.gdn.qa.future.utils.MobileUtil;
import org.springframework.stereotype.Component;

@Component("com.gdn.qa.future.pages.mobile.LoginMobilePage")
public class LoginMobilePage extends MobileUtil {
    public void typeUsername(String emailUser) {
        type(find(LoginAppUI.EMAIL_TEXTBOX), emailUser, 30);
    }

    public void typePassword(String passwordUser) {
        type(find(LoginAppUI.PASSWORD_TEXTBOX), passwordUser, 30);
    }

    public void clickLoginButtonForm() {
        find(LoginAppUI.LOGIN_BUTTON_FORM).click();
    }

    public void clickEmailValidation() {
        find(LoginAppUI.EMAIL_VALIDATION_BUTTON).click();
    }

    public void fillCodeValidation(String verificationCode) {
        type(find(LoginAppUI.OTP_BOX), verificationCode, 30);
    }

    public void clickVerificationBtn() {
        find(LoginAppUI.VERIFICATION_BUTTON).click();
    }

    public void clickNantiSaja() {
        find(LoginAppUI.NANTI_SAJA_VERIFIKASI_NOMOR).click();
        if (find(LoginAppUI.NANTI_SAJA_VERIFIKASI_NOMOR).isDisplayed()){
            find(LoginAppUI.NANTI_SAJA_VERIFIKASI_NOMOR).click();
        }
    }

    public void clickSureVerification() {
        find(LoginAppUI.SURE_NANTI_SAJA_BUTTON).click();
    }

    public String getErrorMessage() {
        return find(LoginAppUI.ERROR_MESSAGE).getText();
    }

    public boolean loginButtonIsEnable() {
        return find(LoginAppUI.LOGIN_BUTTON_FORM).isEnabled();
    }
}
