package com.gdn.qa.future.pages.desktop;

import com.gdn.qa.future.locators.desktop.PopupDesktopUI;
import com.gdn.qa.future.utils.WebElementHelper;
import org.springframework.stereotype.Component;

@Component("com.gdn.qa.future.pages.desktop.PopupPage")
public class PopupPage extends WebElementHelper {
    public boolean getSuccesfulSubscribe() {
        return find(PopupDesktopUI.POPUP_SUCCESSFUL_SUBSCRIBE).isDisplayed();
    }

    public boolean getLihatLanggananButton() {
        return find(PopupDesktopUI.LIHAT_LANGGANAN_BUTTON).isClickable();
    }

    public void clickBuatLanggananBaru() {
        find(PopupDesktopUI.BUAT_LANGGANAN_BARU_BUTTON).click();
    }
}
