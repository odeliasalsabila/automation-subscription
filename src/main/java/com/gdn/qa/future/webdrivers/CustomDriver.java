package com.gdn.qa.future.webdrivers;

import com.gdn.qa.future.utils.EnvironmentUtil;
import lombok.SneakyThrows;
import net.thucydides.core.webdriver.DriverSource;
import org.openqa.selenium.WebDriver;

public class CustomDriver implements DriverSource {
    String browser = EnvironmentUtil.getProperty("browser");
    boolean remote = Boolean.parseBoolean(EnvironmentUtil.getProperty("remote"));

    @SneakyThrows
    @Override
    public WebDriver newDriver() {
        WebDriver driver = null;
        if (remote){
            switch (browser) {
                case "chrome":
                    ChromeDriverManager chromeDriverManager = new ChromeDriverManager();
                    driver = chromeDriverManager.chromeRemote();
                    break;
                case "firefox":
                    FirefoxDriverManager firefoxDriverManager = new FirefoxDriverManager();
                    driver = firefoxDriverManager.firefoxRemote();
                    break;
                default:
                    break;
            }
        }else{
            switch (browser) {
                case "chrome":
                    ChromeDriverManager chromeDriverManager = new ChromeDriverManager();
                    driver = chromeDriverManager.chrome();
                    break;
                case "firefox":
                    FirefoxDriverManager firefoxDriverManager = new FirefoxDriverManager();
                    driver = firefoxDriverManager.firefox();
                    break;
                case "appium":
                    AppiumDriverManager appiumDriverManager = new AppiumDriverManager();
                    driver = appiumDriverManager.appium();
                    break;
                default:
                    break;
            }
        }
        return driver;
    }

    @Override
    public boolean takesScreenshots() {
        return false;
    }
}
