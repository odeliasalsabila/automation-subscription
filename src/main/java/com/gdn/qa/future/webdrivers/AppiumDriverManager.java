package com.gdn.qa.future.webdrivers;

import com.gdn.qa.future.utils.EnvironmentUtil;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

public class AppiumDriverManager {

    public WebDriver appium() {
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability("platformName", EnvironmentUtil.getProperty("platformName"));
        desiredCapabilities.setCapability("automationName", EnvironmentUtil.getProperty("automationName"));
        desiredCapabilities.setCapability("udid", EnvironmentUtil.getProperty("udid"));
        desiredCapabilities.setCapability("systemPort", EnvironmentUtil.getProperty("systemPort"));
        desiredCapabilities.setCapability("noReset", false);
        desiredCapabilities.setCapability("appPackage", "blibli.mobile.commerce");
        desiredCapabilities.setCapability("appActivity", "blibli.mobile.ng.commerce.core.init.view.SplashActivity");

        URL url = null;
        try {
            url = new URL(EnvironmentUtil.getProperty("appiumUrl"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return new AndroidDriver<>(url, desiredCapabilities);
    }
}
