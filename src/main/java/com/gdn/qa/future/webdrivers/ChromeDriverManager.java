package com.gdn.qa.future.webdrivers;

import com.gdn.qa.future.utils.EnvironmentUtil;
import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.SneakyThrows;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;

public class ChromeDriverManager{
    String browser = EnvironmentUtil.getProperty("browser");

    public ChromeDriver chrome(){
        WebDriverManager.chromedriver().setup();
        ChromeDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        return driver;
    }

    @SneakyThrows
    public RemoteWebDriver chromeRemote(){
        String remoteUrl = EnvironmentUtil.getProperty("remoteURL");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setBrowserName(browser.toLowerCase());
        capabilities.setCapability("enableVNC", Boolean.parseBoolean(EnvironmentUtil.getProperty("enableVNC")));
        capabilities.setCapability("enableVideo", Boolean.parseBoolean(EnvironmentUtil.getProperty("enableVideo")));

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setExperimentalOption("prefs", new HashMap<String, Object>() {
            {
                put("profile.default_content_settings.popups", 0);
                put("download.default_directory", "/home/selenium/Downloads");
                put("download.prompt_for_download", false);
                put("download.directory_upgrade", true);
                put("safebrowsing.enabled", false);
                put("plugins.always_open_pdf_externally", true);
                put("plugins.plugins_disabled", new ArrayList<String>() {
                    {
                        add("Chrome PDF Viewer");
                    }
                });
            }
        });
        chromeOptions.merge(capabilities);
        RemoteWebDriver driver = new RemoteWebDriver(URI.create(remoteUrl).toURL(), chromeOptions);
        driver.manage().window().maximize();
        return driver;
    }
}
