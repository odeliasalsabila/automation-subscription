package com.gdn.qa.future.webdrivers;

import com.gdn.qa.future.utils.EnvironmentUtil;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

public class FirefoxDriverManager {
    String browser = EnvironmentUtil.getProperty("browser");

    public FirefoxDriver firefox(){
        WebDriverManager.firefoxdriver().setup();
        FirefoxDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
        return driver;
    }

    public RemoteWebDriver firefoxRemote() throws MalformedURLException {
        String remoteUrl = EnvironmentUtil.getProperty("remoteURL");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setBrowserName(browser.toLowerCase());
        capabilities.setCapability("enableVNC", Boolean.parseBoolean(EnvironmentUtil.getProperty("enableVNC")));
        capabilities.setCapability("enableVideo", Boolean.parseBoolean(EnvironmentUtil.getProperty("enableVideo")));

        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.setCapability("moz:firefoxOptions", new HashMap<String, Object>() {
            {
                put("prefs", new HashMap<String, Object>() {
                    {
                        put("browser.helperApps.neverAsk.saveToDisk", "application/octet-stream");
                    }
                });
            }
        });
        firefoxOptions.merge(capabilities);
        RemoteWebDriver driver = new RemoteWebDriver(new URL(remoteUrl), firefoxOptions);
        driver.manage().window().maximize();
        return driver;
    }
}
