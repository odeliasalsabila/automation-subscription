package com.gdn.qa.future.locators.mobile;

import org.openqa.selenium.By;

public class FavoritSectionAppUI {
    public static final By WIDGET_SEMUA = By.xpath("//android.widget.TextView[@text='Semua']");
    public static final By WIDGET_BLIBLIMART = By.xpath("//android.widget.TextView[@text='BlibliMart']");
}
