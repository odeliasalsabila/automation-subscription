package com.gdn.qa.future.locators.desktop;

import org.openqa.selenium.By;

import java.util.List;

public class LoginDesktopUI {
    public static final By LOGIN_BUTTON = By.xpath("//button[@class='blu-btn btn__login b-outline b-small']");
    public static final By EMAIL_TEXTBOX = By.xpath("//input[@class='form__input login__username']");
    public static final By PASSWORD_TEXTBOX = By.xpath("//input[@class='form__input login__password']");
    public static final By LOGIN_BUTTON_FORM = By.xpath("//div[@class='login__button']//button");
    public static final By FORM_LOGIN = By.xpath("//div[@class='login__wrapper']");
    public static final By EMAIL_VALIDATION_BTN = By.xpath("//button[@class='blu-btn otp-validation__button b-full-width b-secondary']");
    public static final By VERIFICATION_CODE_FORM = By.xpath("//input[@class='otp__textField not-active']");
    public static final By VERIFIKASI_BTN = By.xpath("//button[@class='blu-btn otp__confirm-button b-full-width b-secondary']");
    public static final By NANTI_SAJA_BUTTON = By.xpath("//button[@class='blu-btn footer__btn b-ghost b-secondary']");
    public static final By CLOSE_VERIFIKASI_EMAIL_BUTTON = By.xpath("//div[@class='blu-modal otp__modal b-page b-no-button']//button[@class='blu-modal__close']");
    public static final By BATALKAN_BUTTON = By.xpath("(//button[@class='blu-btn b-secondary'])[2]");
    public static final By WARNING_SESSION = By.xpath("(//div[@class='ticker__msg'])[2]");
}
