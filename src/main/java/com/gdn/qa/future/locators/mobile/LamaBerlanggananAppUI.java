package com.gdn.qa.future.locators.mobile;

import org.openqa.selenium.By;

public class LamaBerlanggananAppUI {
    public static final By PERIOD_BUTTON = By.xpath("//android.view.View[@resource-id='blibli.mobile.commerce:id/tv_drop_down_click_wrapper']");
    public static final String LIST_PERIOD = "//android.widget.TextView[@text='%s']";
    public static final By QUANTITY = By.xpath("//android.widget.EditText[@resource-id='blibli.mobile.commerce:id/et_qty']");
    public static final By TAMBAH_KE_BAG = By.id("blibli.mobile.commerce:id/bt_add_to_bag");
    public static final By SUBSCRIBE_SUCCESS_POPUP = By.xpath("//android.widget.TextView[@text='Kamu berhasil menambahkan produk langganan!']");
    public static final By LIHAT_LANGGANAN_BUTTON = By.id("blibli.mobile.commerce:id/bt_see_subscription");
    private static final String FREQUENCY = "//android.widget.TextView[@text='%s']/preceding-sibling::android.widget.RadioButton[@resource-id='blibli.mobile.commerce:id/rb_frequency']";

    public static By getLamaBerlanggananFrequency(String value) {
        return By.xpath(String.format(FREQUENCY, value));
    }

    public static By getListPeriodSubscription(String value) {
        return By.xpath(String.format(LIST_PERIOD, value));
    }
}
