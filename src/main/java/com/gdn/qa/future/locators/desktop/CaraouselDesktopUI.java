package com.gdn.qa.future.locators.desktop;

import org.openqa.selenium.By;

public class CaraouselDesktopUI {
    public static final By CAROUSEL_XPATH = By.xpath("//div[@class='container']//div[@class='carousel-container']");
}
