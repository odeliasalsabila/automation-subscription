package com.gdn.qa.future.locators.desktop;

import org.openqa.selenium.By;

public class TncPopupDesktopUI {
    public static final By TNC_LINK = By.xpath("//a[@class='circular-head__see-tnc-button']");
    public static final By TNC_POPUP = By.xpath("//div[@class='blu-modal__container']//h3[contains(text(),'Syarat')]");
    public static final By CLOSE_BUTTON = By.xpath("//button[@class='blu-modal__close']");
}
