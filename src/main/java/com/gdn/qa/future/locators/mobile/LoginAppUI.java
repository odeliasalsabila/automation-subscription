package com.gdn.qa.future.locators.mobile;

import org.openqa.selenium.By;

public class LoginAppUI {
    public static final By LOGIN_BUTTON = By.id("blibli.mobile.commerce:id/bt_login");
    public static final By EMAIL_TEXTBOX = By.xpath("//android.widget.EditText[@text='Email/nomor HP']");
    public static final By PASSWORD_TEXTBOX = By.xpath("//android.widget.EditText[@text='Kata sandi']");
    public static final By LOGIN_BUTTON_FORM = By.xpath("//android.widget.Button[@text='Masuk']");
    public static final By EMAIL_VALIDATION_BUTTON = By.id("blibli.mobile.commerce:id/bt_send_to_email");
    public static final By OTP_BOX = By.id("blibli.mobile.commerce:id/et_otp_digit_1");
    public static final By VERIFICATION_BUTTON = By.id("blibli.mobile.commerce:id/btn_submit");
    public static final By NANTI_SAJA_VERIFIKASI_NOMOR = By.id("blibli.mobile.commerce:id/tv_back");
    public static final By SURE_NANTI_SAJA_BUTTON = By.id("android:id/button1");
    public static final By ERROR_MESSAGE = By.id("blibli.mobile.commerce:id/tv_message");
}
