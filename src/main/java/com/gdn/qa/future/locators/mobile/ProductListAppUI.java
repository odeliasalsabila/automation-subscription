package com.gdn.qa.future.locators.mobile;

import org.openqa.selenium.By;

public class ProductListAppUI {
    public static final By MANTAP_TOPRATED_ONBORADING = By.xpath("//android.widget.TextView[@text='Siap']");
    public static final By HIDE_OOS_PRODUCT = By.xpath("//android.view.ViewGroup[@resource-id='blibli.mobile.commerce:id/cl_options']//android.widget.TextView[contains(@text,'stok habis')]");
    public static final By PRODUCT_BLIMART = By.xpath("(//android.widget.TextView[@resource-id='blibli.mobile.commerce:id/tv_product_name'])[1]");
    public static final By TERAPKAN_BUTTON = By.xpath("//android.widget.Button[@text='Terapkan']");
    public static final By HIDE_OOS_PRODUCT_TEXT = By.xpath("//android.widget.TextView[@text='Sembunyikan stok habis']");
    public static final By TAMBAHKAN_KE_BAG_BUTTON = By.xpath("(//android.widget.Button[@resource-id='blibli.mobile.commerce:id/btn_add_to_cart'])[2]");
}
