package com.gdn.qa.future.locators.mobile;

import org.openqa.selenium.By;

public class ProductDetailAppUI {
    public static final By ONBOARDING_SUBS = By.xpath("//android.widget.FrameLayout[@resource-id='blibli.mobile.commerce:id/cl_subscription_action']");
    public static final By OKE_BUTTON_ONBOARDING_SUBS = By.xpath("//android.widget.TextView[@text='Oke']");
    public static final By BERLANGGANAN_BUTTON = By.xpath("//android.widget.FrameLayout[@resource-id='blibli.mobile.commerce:id/cl_subscription_action']//android.view.ViewGroup");
}
