package com.gdn.qa.future.locators.desktop;

import org.openqa.selenium.By;

public class CartDesktopUI {
    private static final String LIST_PRODUCT_NAME = "//span[contains(text(),'%s')]/ancestor::div[contains(@class,'products__wrapper')]//div[@class='products__product-name']";
    public static final By DELETE_CLARIFICATION_BUTTON = By.xpath("(//button[@class='blu-btn b-secondary'])[1]");
    private static final String TRASH_ICON_PRODUCT = "//div[@class='products__product-name'][contains(text(),'%s')]/ancestor::div[@class='products__product-detail']//img[contains(@src,'trash')]";

    public static By trashIconOnProduct(String productName) {
        return By.xpath(String.format(TRASH_ICON_PRODUCT, productName));
    }

    public static By listProductSubs(String bundles) {
        return By.xpath(String.format(LIST_PRODUCT_NAME, bundles));
    }
}
