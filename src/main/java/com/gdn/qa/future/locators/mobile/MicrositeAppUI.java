package com.gdn.qa.future.locators.mobile;

import org.openqa.selenium.By;

public class MicrositeAppUI {
    public static final By LANGGANAN_TAB = By.xpath("//android.widget.ListView//android.view.View");
    public static final By BANNER_SUBS = By.xpath("//android.widget.Image[@text='mobile-subscription-banner-stretched']");
    public static final By HOW_TO_SUBS_SECTION = By.xpath("//android.widget.Image[@text='pilihproduk']");
    public static final By PRODUCT_SET = By.xpath("//android.webkit.WebView[@text='Bliblimart']");
    public static final By TERM_N_COND = By.xpath("//android.widget.Image[@text='pilihproduk']/following-sibling::android.view.View");
    public static final By LIHAT_SEMUA = By.xpath("(//android.widget.Image[@text='pilihproduk']/parent::android.view.View/following-sibling::android.view.View)[2]");
    public static final By HOW_TO_SUBS_TITLE_TEXT = By.xpath("//android.view.View[@text='Cara Berlangganan Bliblimart']/following-sibling::android.widget.Image//following::android.view.View[1]");
}
