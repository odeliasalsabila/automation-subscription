package com.gdn.qa.future.locators.desktop;

import org.openqa.selenium.By;

public class ProductDetailUI {
    public static final By PRODUCT_NAME = By.xpath("//div[@class='product-name']");
    public static final By SUBS_BUTTON = By.xpath("//button[@class='blu-btn start-subscribe b-secondary b-small']");
}
