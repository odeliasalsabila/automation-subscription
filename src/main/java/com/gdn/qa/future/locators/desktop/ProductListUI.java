package com.gdn.qa.future.locators.desktop;

import org.openqa.selenium.By;

public class ProductListUI {
    public static final By HIDE_OOS = By.xpath("(//input[@id='Sembunyikan stok habis']/following-sibling::div)[1]");
    public static final By PRODUCT_NAME = By.xpath("(//div[@class='product__title__name'])[1]");
    public static final By PRODUCT_SET = By.xpath("//div[@class='blu-card productset-item']");
    public static final By HIDE_OOS_LABEL = By.xpath("(//div[@class='multi-select-filter oos-filter']//label)[1]");
}
