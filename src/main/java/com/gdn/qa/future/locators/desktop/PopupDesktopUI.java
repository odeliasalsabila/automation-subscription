package com.gdn.qa.future.locators.desktop;

import org.openqa.selenium.By;

public class PopupDesktopUI {
    public static final By CLOSE_BUTTON = By.xpath("//img[@alt='close']");
    public static final By CLOSE_BUTTON_IFRAME = By.xpath("//i[contains(@id,'close-button')]");
    public static final By IFRAME = By.xpath("//iframe[contains(@class,'sp-fancybox-iframe sp-fancybox-skin sp-fancybox-iframe')]");
    public static final By NANTI_SAJA_ACCESS_LOCATION = By.xpath("//button[@class='blu-btn decline-btn b-outline b-secondary']");
    public static final By POPUP_SUCCESSFUL_SUBSCRIBE = By.xpath("//div[@class='subscription-modal-success__title']");
    public static final By LIHAT_LANGGANAN_BUTTON = By.xpath("//div[@class='subscription-modal-success__button']//button[@class='blu-btn b-secondary']");
    public static final By BUAT_LANGGANAN_BARU_BUTTON = By.xpath("//button[@class='blu-btn b-outline b-secondary']");
}
