package com.gdn.qa.future.locators.desktop;

import org.openqa.selenium.By;

public class SearchBarUI {
    public static final By SEARCH_BAR = By.xpath("//input[@name='search']");
    public static final By SEARCH_ICON = By.xpath("//button[@class='searchbox__search']");
}
