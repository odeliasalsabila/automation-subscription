package com.gdn.qa.future.locators.mobile;

import org.openqa.selenium.By;

public class TermCondSubsAppUI {
    public static final By TERM_N_COND = By.xpath("(//android.view.View)[2]");
    public static final By CLOSE_ICON = By.xpath("(//android.view.View)[3]");
}
