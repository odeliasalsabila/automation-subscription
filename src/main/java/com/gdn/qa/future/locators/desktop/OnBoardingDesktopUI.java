package com.gdn.qa.future.locators.desktop;

import org.openqa.selenium.By;

public class OnBoardingDesktopUI {
    public static final By SIAP_TWO_DELIVERY_BUTTON = By.xpath("(//button[@class='twoHourDelivery_onboarding_content_button_next'])[6]");
    public static final By MANTAP_TOPRATED_ONBORADING = By.xpath("//button[@class='topRated_onboarding_content_button_next']");
    public static final By SUBS_ONBOARDING = By.xpath("//div[@id='subscription-content']");
    public static final By OKE_SUBS_ONBOARDING = By.xpath("//button[@class='subscription-onboarding__content--button']");
    public static final By SIAP_BUTTON = By.xpath("//button[@class='subscription-onboarding__content--button']");
}
