package com.gdn.qa.future.locators.mobile;

import org.openqa.selenium.By;

public class AccountMobileUI {
    public static final By AKUN_ICON = By.id("blibli.mobile.commerce:id/navigation_account");
    public static final By USER_INFO = By.id("blibli.mobile.commerce:id/vw_layout_header");
    public static final By USERNAME = By.xpath("(//android.widget.EditText)[2]");
}
