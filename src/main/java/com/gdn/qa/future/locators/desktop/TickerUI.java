package com.gdn.qa.future.locators.desktop;

import org.openqa.selenium.By;

public class TickerUI {
    public static final By TICKER_ERROR = By.xpath("//article[@class='ticker danger has-icon']");
    public static final By TICKER_ERROR_MESSAGE = By.xpath("//article[@class='ticker danger has-icon']//div[@class='ticker__msg']");
}
