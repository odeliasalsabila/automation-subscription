package com.gdn.qa.future.locators.desktop;

import org.openqa.selenium.By;

public class FavoritSectionUI {
    public static final By SEMUA_MENU = By.xpath("(//div[@class='favourite__item'])[1]");
    public static final By BLIBLIMART_CATEGORY = By.xpath("//div[@class='favourite-modal__content__tab-container__favourite-list']/img[@alt='BlibliMart']");
}
