package com.gdn.qa.future.locators.desktop;

import org.openqa.selenium.By;

public class LanggananUI {
    public static final By LANGGANAN_TAB = By.xpath("//div[@class='list-cat-link']//a[contains(text(),'Langganan')]");
    public static final By SUBSCRIPTION_BANNER = By.xpath("//img[@class='lazyImage']");
    public static final By HOW_TO_SUBS_SECTION = By.xpath("//div[@class='circular-image__title'][contains(text(),'Cara Berlangganan')]/following-sibling::div");
    public static final By LIHAT_SEMUA = By.xpath("(//a[@class='productset__see-all'])[1]");
    public static final By HOW_TO_SUBS_TITLE_TEXT = By.xpath("//p[@class='tx-bold']");
}
