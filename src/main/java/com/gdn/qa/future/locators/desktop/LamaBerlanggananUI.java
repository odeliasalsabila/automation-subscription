package com.gdn.qa.future.locators.desktop;

import org.openqa.selenium.By;

import java.util.List;

public class LamaBerlanggananUI {
    public static final By PERIOD_BUTTON = By.xpath("//div[@class='blu-dropdown__trigger']");
    public static final By LIST_PERIOD = By.xpath("//button[contains(@class,'blu-list__item')]");
    public static final By QUANTITY_SUBSCRIPTION = By.xpath("//div[@class='subs-quantity']//input");
    public static final By INCREASE_QUANTITY_BUTTON = By.xpath("//button[@class='blu-btn subs-quantity__increase b-outline b-secondary b-small']");
    public static final By DECREASE_QUANTITY_BUTTON = By.xpath("//button[@class='blu-btn subs-quantity__decrease b-outline b-disabled b-secondary b-small']");
    public static final By TAMBAH_KE_LANGGANAN_BUTTON = By.xpath("//button[@class='blu-btn b-full-width b-secondary']");
    public static final By LIHAT_LANGGANAN_BUTTON = By.xpath("//div[@class='subscription-modal-success__button']//button");
    public static final By PRICE = By.xpath("//span[@class='tx-primary subs-modal__subs-price']");
    public static final By FREQUENCY_CHECKED_SUBSCRIPTION = By.xpath("//div[@class='subs-modal__radio-element']//div[@class='blu-radio b-checked']");
    public static final By PERIOD_SUBSCRIPTION = By.xpath("//div[@class='subs-modal__radio-element']//div[@class='blu-radio b-checked']/ancestor::div//span[@class='blu-dropdown__value']");
    private static final String FREQUENCY = "//label[@class='blu-radio-label'][contains(text(),'%s')]";

    public static By getLamaBerlanggananFrequency(String values) {
        return By.xpath(String.format(FREQUENCY, values));
    }
}
