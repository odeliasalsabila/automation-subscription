package com.gdn.qa.future.api;

import com.gdn.qa.future.properties.DefaultProperties;
import com.gdn.qa.future.properties.ServiceProperties;
import io.restassured.specification.RequestSpecification;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

import static net.serenitybdd.rest.SerenityRest.given;

public class ServiceApi {
    @Autowired
    ServiceProperties serviceProperties;

    @Autowired
    DefaultProperties defaultProperties;

    public ServiceApi(){
    }

    public RequestSpecification service(String serviceName){
        RequestSpecification requestSpecification = given();
        addBaseUrl(serviceName, requestSpecification);
        addHeader(requestSpecification);
        addCookies(requestSpecification);
        return requestSpecification;
    }

    private void addBaseUrl(String serviceName, RequestSpecification requestSpecification) {
        String baseUrl = serviceProperties.getBaseUrl().get(serviceName);
        requestSpecification.baseUri(baseUrl);
    }

    private void addHeader(RequestSpecification requestSpecification){
        for (Map.Entry<String, String> entry : serviceProperties.getHeaders().entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();

            requestSpecification.header(key,value);
        }
    }

    private void addCookies(RequestSpecification requestSpecification){
        for (Map.Entry<String, String> entry : defaultProperties.getCookies().entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();

            requestSpecification.cookie(key,value);
        }
    }

}
