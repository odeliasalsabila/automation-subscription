package com.gdn.qa.future.api.controller;

import com.gdn.qa.future.api.ServiceApi;
import com.gdn.qa.future.api.response.getSubscriptionProduct.GetSubscriptionProductResponse;
import com.gdn.qa.future.data.api.SubscriptionProductApiData;
import com.gdn.qa.future.data.api.UserLoginApiData;
import io.restassured.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("com.gdn.qa.future.api.controller.SubscriptionProductController")
public class SubscriptionProductController extends ServiceApi {
    @Autowired
    UserLoginApiData userLoginApiData;

    @Autowired
    SubscriptionProductApiData subscriptionProductApiData;

    public GetSubscriptionProductResponse getSubscriptionProductResponse() {
        Response response = service("blibli")
                .pathParams("itemSku", subscriptionProductApiData.getItemSku())
                .when()
                .cookies(userLoginApiData.getCookies())
                .get("/backend/subscription/products/{itemSku}");
        response.body().prettyPrint();
        return response.getBody().as(GetSubscriptionProductResponse.class);
    }
}
