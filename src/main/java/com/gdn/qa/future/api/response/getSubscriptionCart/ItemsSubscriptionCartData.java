package com.gdn.qa.future.api.response.getSubscriptionCart;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ItemsSubscriptionCartData {
    private String itemSku;
    private String name;
}
