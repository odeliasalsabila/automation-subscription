package com.gdn.qa.future.api.controller;

import com.gdn.qa.future.api.ServiceApi;
import com.gdn.qa.future.api.response.getLoginUser.GetUserLoginResponse;
import com.gdn.qa.future.data.api.UserLoginApiData;
import io.restassured.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("com.gdn.qa.future.api.controller.UserController")
public class UserController extends ServiceApi {
    @Autowired
    UserLoginApiData userLoginApiData;

    public GetUserLoginResponse getUserHasloggedIn() {
        Response response = service("blibli").
                when().
                cookies(userLoginApiData.getCookies()).
                get("/backend/common/users");
        response.body().prettyPrint();
        return response.getBody().as(GetUserLoginResponse.class);
    }
}
