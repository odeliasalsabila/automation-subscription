package com.gdn.qa.future.api.response.getSubscriptionCart;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubscriptionCartData {
    private List<ItemsSubscriptionCartData> items;
}
