package com.gdn.qa.future.api.response.getProductDetails;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetProductDetailsResponse {
    private int code;
    private String status;
    private ProductDetailsData data;
}
