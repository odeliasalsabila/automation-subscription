package com.gdn.qa.future.api.response.getSubsSection;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubsSectionParameter {
    private String title;
    private String type;
}
