package com.gdn.qa.future.api.response.getProductDetails;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductDetailsData {
    private String[] tags;
}
