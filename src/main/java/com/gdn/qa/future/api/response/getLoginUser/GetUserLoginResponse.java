package com.gdn.qa.future.api.response.getLoginUser;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetUserLoginResponse {
    private int code;
    private String status;
    private UserLoginData data;
}
