package com.gdn.qa.future.api.response.getSubscriptionProduct;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubscriptionProductData {
    private String name;
    private String itemSku;
}
