package com.gdn.qa.future.api.controller;

import com.gdn.qa.future.api.ServiceApi;
import com.gdn.qa.future.api.response.getSubsSection.GetSubsSectionResponse;
import io.restassured.response.Response;
import org.springframework.stereotype.Component;

@Component("com.gdn.qa.future.api.controller.SubscriptionSectionController")
public class SubscriptionSectionController extends ServiceApi {
    public GetSubsSectionResponse getSubsSectionResponse() {
        Response response = service("blibli")
                .when()
                .get("/backend/content/pages/blm-subs/sections?display=mobile");
        response.body().prettyPrint();
        return response.getBody().as(GetSubsSectionResponse.class);
    }
}
