package com.gdn.qa.future.api.response.getSubsSection;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubsSectionBlock {
    private String id;
    private int sequence;
    private List<SubsSectionComponent> components;
}
