package com.gdn.qa.future.api.controller;

import com.gdn.qa.future.api.ServiceApi;
import com.gdn.qa.future.api.request.SubscriptionCartRequest;
import com.gdn.qa.future.api.response.getSubscriptionCart.AddSubscriptionCartResponse;
import com.gdn.qa.future.data.api.SubscriptionCartApiData;
import com.gdn.qa.future.data.api.UserLoginApiData;
import io.restassured.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("com.gdn.qa.future.api.controller.SubscriptionCartController")
public class SubscriptionCartController extends ServiceApi {
    @Autowired
    private SubscriptionCartApiData subscriptionCartApiData;

    @Autowired
    private UserLoginApiData userLoginApiData;

    public AddSubscriptionCartResponse addSubscriptionToCartResponse() {
        SubscriptionCartRequest subscriptionCartRequest = new SubscriptionCartRequest();
        subscriptionCartRequest.setItemSku(subscriptionCartApiData.getItemSku());
        subscriptionCartRequest.setPrice(subscriptionCartApiData.getPrice());
        subscriptionCartRequest.setQuantity(subscriptionCartApiData.getQuantity());
        subscriptionCartRequest.setFrequency(subscriptionCartApiData.getFrequency());
        subscriptionCartRequest.setPeriod(subscriptionCartApiData.getPeriod());

        Response response = service("blibli")
                .when()
                .cookies(userLoginApiData.getCookies())
                .body(subscriptionCartRequest)
                .post("/backend/subscription/carts");
        response.body().prettyPrint();
        return response.getBody().as(AddSubscriptionCartResponse.class);
    }
}
