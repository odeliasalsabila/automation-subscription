package com.gdn.qa.future.api;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.mail.*;
import javax.mail.internet.MimeBodyPart;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

@Component("com.gdn.qa.future.api.MailReading")
public class MailReading {
    private final String saveDirectory = System.getProperty("user.dir") + "\\SaveEmails";

    public Object readInboxMail(String subject, String email, String passGmail) {
        Properties properties = new Properties();
        properties.setProperty("mail.store.protocol", "imaps");
        try {
            Session session = Session.getDefaultInstance(properties, null);

            Store store = session.getStore("imaps");
            store.connect("imap.gmail.com", email, passGmail);
            Folder inbox = store.getFolder("INBOX");

            //Unread Mails
            int unreadMailCount = inbox.getUnreadMessageCount();

            inbox.open(Folder.READ_WRITE);

            //Total Mails
            Message[] messages = inbox.getMessages();

            Object referredMail = null;
            for (int i = messages.length; i > (messages.length - unreadMailCount); i--) {
                Message message = messages[i - 1];

                Address[] from = message.getFrom();
                System.out.println("Date: " + message.getSentDate());
                System.out.println("From: " + from[0]);
                System.out.println("Subject: " + message.getSubject());
                if (message.getSubject().equalsIgnoreCase(subject)) {
                    String contentType = message.getContentType();
                    // store attachment file name, separated by comma
                    StringBuilder attachFiles = new StringBuilder();

                    if (contentType.contains("multipart")) {
                        // content may contain attachments
                        Multipart multiPart = (Multipart) message.getContent();
                        int numberOfParts = multiPart.getCount();
                        for (int partCount = 0; partCount < numberOfParts; partCount++) {
                            MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(partCount);
                            if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {
                                // this part is attachment
                                String fileName = part.getFileName();
                                attachFiles.append(fileName).append(", ");
                                part.saveFile(saveDirectory + File.separator + fileName);
                            } else {
                                // this part may be the message content
                                System.out.println("Message Content: " + part.getContent().toString());
                            }
                        }

                        if (attachFiles.length() > 1) {
                            attachFiles = new StringBuilder(attachFiles.substring(0, attachFiles.length() - 2));
                        }
                    } else if (contentType.contains("TEXT/PLAIN")
                            || contentType.contains("TEXT/HTML")) {
                        Object content = message.getContent();
                        if (content != null) {
                            System.out.println("Message Content: " + content.toString());
                        }
                    }
                    System.out.println("Attachments: " + attachFiles);

                    System.out.println("====================================== Mail no.: " + i + " end ======================================");
                    referredMail = message.getContent();
                    break;
                }
            }

            // disconnect
            inbox.close(false);
            store.close();
            return referredMail;
        } catch (NoSuchProviderException ex) {
            System.out.println("No provider for pop3.");
            ex.printStackTrace();
        } catch (MessagingException ex) {
            System.out.println("Could not connect to the message store");
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public String readVerificationCode(String email, String passwordGmail) {
        String subject = "Verifikasi untuk masuk akun Blibli dari perangkat baru";
        Object referredEmail = readInboxMail(subject, email, passwordGmail);
        if (referredEmail != null) {
            String messageContent = referredEmail.toString();
            return StringUtils.substringBetween(messageContent, "\"font-size:24px;\">", "</strong>");
        }
        return null;
    }
}
