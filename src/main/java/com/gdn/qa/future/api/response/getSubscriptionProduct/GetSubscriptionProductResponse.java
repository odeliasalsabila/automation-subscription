package com.gdn.qa.future.api.response.getSubscriptionProduct;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetSubscriptionProductResponse {
    private int code;
    private String status;
    private SubscriptionProductData data;
}
