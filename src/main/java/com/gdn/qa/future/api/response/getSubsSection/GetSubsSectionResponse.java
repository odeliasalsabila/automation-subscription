package com.gdn.qa.future.api.response.getSubsSection;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetSubsSectionResponse {
    private int code;
    private String status;

    private List<SubsSectionData> data;
}
