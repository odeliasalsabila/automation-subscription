package com.gdn.qa.future.api.controller;

import com.gdn.qa.future.api.ServiceApi;
import com.gdn.qa.future.api.response.getProductDetails.GetProductDetailsResponse;
import com.gdn.qa.future.data.api.ProductDetailsApiData;
import io.restassured.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("com.gdn.qa.future.api.controller.ProductDetailController")
public class ProductDetailController extends ServiceApi {
    @Autowired
    ProductDetailsApiData productDetailsApiData;

    public GetProductDetailsResponse getProductDetailRequest() {
        Response response = service("blibli")
                .pathParams("productCode", productDetailsApiData.getProductCode())
                .pathParams("productSku", productDetailsApiData.getProductSku())
                .when()
                .get("/backend/product-detail/products/pc--{productCode}/_summary?defaultItemSku={productSku}");
        response.body().prettyPrint();
        return response.getBody().as(GetProductDetailsResponse.class);
    }
}
