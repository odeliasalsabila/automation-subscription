package com.gdn.qa.future.api.request;

import lombok.Data;

@Data
public class SubscriptionCartRequest {
    private String itemSku;
    private int price;
    private int quantity;
    private String frequency;
    private int period;
}
