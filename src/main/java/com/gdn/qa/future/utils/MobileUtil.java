package com.gdn.qa.future.utils;

import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.webdriver.ThucydidesWebDriverSupport;
import net.thucydides.core.webdriver.WebDriverFacade;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class MobileUtil extends PageObject {
    public int startx, starty, endx;

    public static AndroidDriver getCurrentDriver(){
        return (AndroidDriver) ((WebDriverFacade) ThucydidesWebDriverSupport.getDriver()).getProxiedDriver();
    }

    public void click(WebElementFacade element, int timeOut) {
        element.withTimeoutOf(timeOut, TimeUnit.SECONDS).waitUntilVisible();
        element.click();
    }

    public void type(WebElementFacade element, String text, int timeOut) {
        fluentWait(element, timeOut);
        element.type(text);
    }

    public void fluentWait(WebElementFacade element, int timeOut) {
        element.withTimeoutOf(timeOut, TimeUnit.SECONDS).waitUntilVisible();
    }

    public String getText(WebElementFacade element, int timeOut) {
        fluentWait(element, timeOut);
        return element.getText();
    }

    public static void scrollUpTo() {
        Dimension size;
        size = getCurrentDriver().manage().window().getSize();
        Double screenHeightStart = size.getHeight() * 0.5;
        int scrollStart = screenHeightStart.intValue();
        Double screenHeightEnd = size.getHeight() * 0.2;
        int scrollEnd = screenHeightEnd.intValue();
        swipe(0, scrollStart, 0, scrollEnd, 2000);
    }

    public void scrollUpByCount(int count) {
        for (int i = 0; i < count; i++) {
            Dimension size;
            size = getCurrentDriver().manage().window().getSize();
            Double screenHeightStart = size.getHeight() * 0.75;
            int scrollStart = screenHeightStart.intValue();
            Double screenHeightEnd = size.getHeight() * 0.2;
            int scrollEnd = screenHeightEnd.intValue();
            swipe(0, scrollStart, 0, scrollEnd, 2000);
        }
    }

    public static void scrollDownTo() {
        Dimension size;
        size = getCurrentDriver().manage().window().getSize();
        Double screenHeightStart = size.getHeight() * 0.5;
        int scrollStart = screenHeightStart.intValue();
        Double screenHeightEnd = size.getHeight() * 0.2;
        int scrollEnd = screenHeightEnd.intValue();
        swipe(0, scrollEnd, 0, scrollStart, 2000);
    }

    public void scrollDownToElement(WebElementFacade elementIndicator, int count) {
        int i = 0;
        do {
            boolean isPresent = isVisible(elementIndicator);
            if (isPresent) {
                break;
            } else {
                scrollDownTo();
            }
            i++;
        } while (i <= count);
    }

    public static void swipe(int startx, int starty, int endx, int endy, int duration) {
        System.out.println(
                "Swipe (" + startx + "," + starty + "," + endx + "," + endy + "," + duration + ")");
        new TouchAction((PerformsTouchActions) getCurrentDriver()).press(new PointOption().withCoordinates(startx, starty))
                .waitAction(new WaitOptions().withDuration(Duration.ofMillis(duration)))
                .moveTo(new PointOption().withCoordinates(endx, endy)).release().perform();
    }

    public void scrollToElement(WebElementFacade elementIndicator, int count) {
        int i = 0;
        do {
            boolean isPresent = isVisible(elementIndicator);
            if (isPresent) {
                break;
            } else {
                scrollUpTo();
            }
            i++;
        } while (i <= count);
    }

    public boolean isVisible(WebElementFacade element) {
        return element.isVisible();
    }

    public void clearText(WebElementFacade element, int timeOut) {
        element.withTimeoutOf(timeOut, TimeUnit.SECONDS).waitUntilVisible();
        element.clear();
    }

    public void clickByCoordinatesUsingId(WebElementFacade elementId, int x, int y) {
        Point point = elementId.getLocation();
        System.out.println("clicked on :" + point.x + " - " + point.y);
        int xCoordinate = point.x + x;
        int yCoordinate = point.y + y;
        TouchAction a2 = new TouchAction((PerformsTouchActions) getCurrentDriver());
        System.out.println("clicked on :" + xCoordinate + " - " + yCoordinate);
        a2.tap(PointOption.point(xCoordinate, yCoordinate)).perform();
    }

    protected void clickElementJavascript(WebElement element){
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("arguments[0].click();", element);
    }
}
