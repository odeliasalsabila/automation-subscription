package com.gdn.qa.future.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

@Component("com.gdn.qa.future.utils.OpenCsvUtil")
public class csvUtil {
    private static final String PATH = System.getProperty("user.dir") + "/target/";

    @SneakyThrows
    public <T> void writeFile(Class<T> classOfT, Object object, String filename) {
            File file = new File(PATH+filename+".csv");

            CsvMapper mapper = new CsvMapper();
            mapper.configure(JsonGenerator.Feature.IGNORE_UNKNOWN, true);

            CsvSchema schema = mapper.schemaFor(classOfT).withHeader();

            mapper.writer(schema).writeValue(file, object);
    }

    @SneakyThrows
    public <T> List<Object> readFile(Class<T> classOfT, String filename) {
        CsvMapper mapper = new CsvMapper();
        CsvSchema schema = CsvSchema.emptySchema().withHeader();

        List<Object> datas = new ArrayList<>();

        ObjectReader objectReader = mapper.readerFor(classOfT).with(schema);
        try (Reader reader = new FileReader( PATH+filename+".csv")) {
            MappingIterator<Object> mi = objectReader.readValues(reader);
            while (mi.hasNext()) {
                Object current = mi.next();
                datas.add(current);
            }
        }
        return datas;
    }
}
