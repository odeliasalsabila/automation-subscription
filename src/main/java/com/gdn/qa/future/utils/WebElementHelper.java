package com.gdn.qa.future.utils;

import net.serenitybdd.core.steps.UIInteractionSteps;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class WebElementHelper extends UIInteractionSteps {
    public void addCookie(HashMap<String, String> cookies) {
        for (Map.Entry<String, String> entry : cookies.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();

            Cookie cookie = new Cookie(key, value);
            getDriver().manage().addCookie(cookie);
        }
    }

    protected void clickElementJavascript(WebElement element){
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("arguments[0].click();", element);
    }

    protected void scrollIntoView(WebElement webElement) {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("arguments[0].scrollIntoView();", webElement);
    }

    protected String getWindowHandle() {
        return getDriver().getWindowHandle();
    }

    protected boolean waitForNewWindow(WebDriver driver, int timeout) {
        boolean flag = false;
        int counter = 0;
        while (!flag) {
            try {
                Set<String> winId = driver.getWindowHandles();
                if (winId.size() > 1) {
                    flag = true;
                    return flag;
                }
                Thread.sleep(1000);
                counter++;
                if (counter > timeout) {
                    return flag;
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
                return false;
            }
        }
        return flag;
    }

    protected void windowsHandleCurrent(String winHandleBefore) {
        for (String winHandle : getDriver().getWindowHandles()) {
            if (!winHandle.equals(winHandleBefore)) {
                getDriver().switchTo().window(winHandle);
            }
        }
    }
}
